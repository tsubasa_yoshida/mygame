#pragma once

#include <string>
#include <Xact3.h>

using namespace std;

//H
namespace Audio
{
	void Initialize();
	void WaveBankLoad(string fireName);
	void SoundBankLoad(string fileName);
	void WaveLoop(string cueName);
	void WaveStop(string cueName);
	void AllRelease();
}