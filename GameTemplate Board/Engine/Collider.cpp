#include "Collider.h"
#include "Direct3D.h"
#include "../Ball.h"

#define IKD_EPSIRON 0.00001f // 誤差

//球体コライダコンストラクタ
Collider::Collider(IGameObject* owner, D3DXVECTOR3 center, float radius)
{
	owner_ = owner;
	center_ = center;
	radius_ = radius;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateSphere(Direct3D::pDevice, radius, 8, 4, &pMesh_, 0);
#endif
}

//平面コライダコンストラクタ
Collider::Collider(IGameObject* owner, D3DXVECTOR3 center, D3DXVECTOR3 normal)
{
	owner_ = owner;
	center_ = center;
	normal_ = normal;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice, 1, 1, 1, &pMesh_, 0);
#endif
}

//箱型コライダコンストラクタ
//Collider::Collider(IGameObject* owner, D3DXVECTOR3 center, D3DXVECTOR3 size)
//{
//	owner_ = owner;
//	center_ = center;
//	size_ = size;
//}


Collider::~Collider()
{
}

void Collider::SetPos(D3DXVECTOR3 pos)
{
	center_ = pos;
}

void Collider::SetPrePos(D3DXVECTOR3 prepos)
{
	prePos_ = prepos;
}

void Collider::SetNormal(D3DXVECTOR3 normal)
{
	normal_ = normal;
}

void Collider::SetRotate(D3DXMATRIX mat)
{
	D3DXVECTOR3 N;
	D3DXVec3Normalize(&N, &normal_);
	D3DXVec3TransformCoord(&normal_, &N, &mat);
}

D3DXVECTOR3 Collider::GetNormal()
{
	D3DXVECTOR3 N;
	D3DXVec3Normalize(&N, &normal_);
	return N;
}

bool Collider::IsHit(Collider& target)
{
	//自分と相手との中心間のベクトルをvに求める
	D3DXVECTOR3 v = ( center_ + owner_->GetPosition()) - ( target.center_ + target.owner_->GetPosition());
	//vの長さをlengthに求める
	float length = D3DXVec3Length(&v);

	//もし自分と相手との半径距離より値が小さければ
	if (length <= radius_ + target.radius_)
	{
		return true;
	}
	return false;
}

bool Collider::CalcParticlePlane(Collider& plane, Collider& particle)
{
	FLOAT t;

	// 平面上の一点から現在位置へのベクトル
	D3DXVECTOR3 vec = particle.GetPosition() - plane.GetPosition();
	
	// 現在位置から予定位置までのベクトル
	D3DXVECTOR3 moveVec = (particle.GetPosition() + particle.owner_->GetPosition()) - particle.prePos_;
	
	//法線
	D3DXVECTOR3 normal;

	//法線の正規化
	D3DXVec3Normalize(&normal, &plane.GetNormal());

	// 平面と中心点の距離を算出
	FLOAT length = D3DXVec3Dot(&vec, &normal);
	FLOAT lengthAbs = fabs(length);

	// 進行方向と法線の関係をチェック
	FLOAT dot = D3DXVec3Dot(&vec, &normal);

	// 平面と平行に移動してめり込んでいるスペシャルケース
	if ((IKD_EPSIRON - fabs(dot) > 0.0f) && (lengthAbs < particle.GetRadius()))
	{
		//ずっと抜け出せないので
		t = FLT_MAX;

		//衝突位置は今の位置に指定
		particle.owner_->SetPosition(particle.prePos_);

		return true;
	}

	t = (particle.GetRadius() - length) / dot;

	// めり込んでいたら衝突として処理終了
	if (lengthAbs < particle.GetRadius())
		return true;

	// 壁に対して移動が逆向きなら衝突していない
	if (dot >= 0)
		return false;

	// 時間が0〜1の間にあれば衝突
	if ((0 <= t) && (t <= 1))
		return true;

	return false;
}



bool Collider::IsHitBoxVsParticle(Collider & target)
{
	D3DXVECTOR3 particlePos = center_ + owner_->GetPosition();
	D3DXVECTOR3 boxPos = target.owner_->GetPosition() + target.center_;

	if (particlePos.x > boxPos.x - target.size_.x - radius_ &&
		particlePos.x < boxPos.x + target.size_.x + radius_ &&
		particlePos.y > boxPos.y - target.size_.y - radius_ &&
		particlePos.y < boxPos.y + target.size_.y + radius_ &&
		particlePos.z > boxPos.z - target.size_.z - radius_ &&
		particlePos.z < boxPos.z + target.size_.z + radius_
		)
	{
		return true;
	}

	return false;
}

bool Collider::IsHitBoxVsBox(Collider & target)
{
	D3DXVECTOR3 boxPosA = center_ + owner_->GetPosition();
	D3DXVECTOR3 boxPosB = target.center_ + target.owner_->GetPosition();

	if ((boxPosA.x + size_.x / 2) > (boxPosB.x - target.size_.x / 2) &&
		(boxPosA.x - size_.x / 2) < (boxPosB.x + target.size_.x / 2) &&
		(boxPosA.y + size_.y / 2) > (boxPosB.y - target.size_.y / 2) &&
		(boxPosA.y - size_.y / 2) < (boxPosB.y + target.size_.y / 2) &&
		(boxPosA.z + size_.z / 2) > (boxPosB.z - target.size_.z / 2) &&
		(boxPosA.z - size_.z / 2) < (boxPosB.z + target.size_.z / 2))
	{
		return true;
	}

	return false;
}

bool Collider::IsHitBoxVsParticleOBB(Collider & target)
{
	//長さを格納するベクトル
	D3DXVECTOR3 vec(0, 0, 0);



	return false;
}

void Collider::Draw(D3DXVECTOR3 position, D3DXVECTOR3 rotate)
{
	//移動行列を作成
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, position.x , position.y , position.z );

	D3DXMATRIX matRX, matRY, matRZ;

	//回転行列の作成
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate.z));

	//変形行列を渡す
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &(matRX * matRY * matRZ * mat));
	pMesh_->DrawSubset(0);
}

