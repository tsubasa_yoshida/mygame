#pragma once
#include "Global.h"

//グローバル関数的な扱い、関数名の衝突を避けるため
namespace Direct3D
{
	extern LPDIRECT3D9	pD3d;	   //Direct3Dオブジェクト
	extern LPDIRECT3DDEVICE9	pDevice;	//Direct3Dデバイスオブジェクト

	//初期化処理
	//引数hWnd	ウィンドウハンドル
	void Initialize(HWND hWnd);	

	void BeginDraw();	//描画開始処理

	void EndDraw();		//描画終了処理

	void Release();		//ポインタの開放処理
}