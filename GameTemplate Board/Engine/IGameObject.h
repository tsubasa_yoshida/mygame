#pragma once
#include <d3dx9.h>
#include <vector>
#include <list>
#include <assert.h>


class Collider;

class Wall;

class Ground;

class IGameObject
{
protected:
	IGameObject* pParent_;
	std::vector<IGameObject*>	childList_;
	std::string name_;

	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	//行列保持用メンバ変数(親から見た)
	//自分の行列を作る
	D3DXMATRIX localMatrix_;

	//行列保持用メンバ変数(世界から見た)
	//親のworldMatrix_と自分のlocalMatrixをかける
	D3DXMATRIX worldMatrix_;

	//生死判断用
	bool dead_;

	//底辺位置到達確認用
	bool lowFlag_;

	//複数使う場合はvectorなどで配列化
	Collider* pCollider_;

	//平面用
	Collider* pPlaneCollider_;

	//箱型
	Collider* pBoxCollider_;


	void Transform();
private:

public:
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);

	virtual ~IGameObject();

	//初期化
	virtual void Initialize() = 0;

	//更新
	virtual void Update() = 0;

	//描画
	virtual void Draw() = 0;

	//開放
	virtual void Release() = 0;

	//当たり判定用仮想関数
	//各クラスで当たった時の処理を記述
	virtual void OnCollision(IGameObject* pTarget, Collider* collTarget, Collider* collThis) {};


	//更新
	void UpdateSub();

	//描画
	void DrawSub();

	//開放
	void ReleaseSub();

	//削除
	void KillMe();

	//当たり判定付与関数
	void SetCollider(D3DXVECTOR3& center, float radius);

	//平面の当たり判定付与
	void SetPlaneCollider(D3DXVECTOR3& center, D3DXVECTOR3& Normal);

	//箱の当たり判定付与
	void SetBoxCollider(D3DXVECTOR3& center, D3DXVECTOR3& size);

	//当たり判定用再起処理関数
	void Collision(IGameObject* targetObject);

	//テスト用の衝突判定枠を表示
	void CollisionDraw();

	template <class T>
	T* CreateGameObject(IGameObject* parent)
	{
		T* p = new T(parent);
		assert(p != nullptr);
		parent->PushBackChild(p);
		p->Initialize();
		return p;
	}

	IGameObject* FindChildObject(const std::string& name);

	void PushBackChild(IGameObject* pObj);

	//セッター
	void SetPosition(D3DXVECTOR3 position) { position_ = position; }
	void SetRotate(D3DXVECTOR3 rotate) { rotate_ = rotate; }
	void SetScale(D3DXVECTOR3 scale) { scale_ = scale; }
	void SetLowFlag(bool flag) { lowFlag_ = flag; }

	//ゲッター
	D3DXVECTOR3 GetPosition() { return position_; }
	D3DXVECTOR3 Getrotate() { return rotate_; }
	D3DXVECTOR3 Getscale() { return scale_; }
	IGameObject* GetParent() { return pParent_; }
	Collider* GetCollider() { return pCollider_; }
	Collider* GetPlaneCollider() { return pPlaneCollider_; }
	//std::vector<Collider*> GetPlaneCollider() { return pPlaneCollider_; }
	std::string& GetObjectName() { return name_; }
	bool GetLowFlag() { return lowFlag_; }
	
};

