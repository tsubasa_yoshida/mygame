#include <Windows.h>
#include "Global.h"	//Direct3Dでインクルードされているので削除してもOK
#include "Direct3D.h"
#include "RootJob.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"

#ifdef _DEBUG
#include <crtdbg.h>
#endif

//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

//定数
const char* WIN_CLASS_NAME = "ゲーム";	//名前を二回以上書く場合は間違える可能性があるため定数化
const int	WINDOW_WIDTH = 800;	 //ウィンドウの幅
const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//グローバル変数
Global g;

RootJob *pRootJob;

//エントリーポイント
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	

	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	
	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);						//この構造体のサイズ
	wc.hInstance = hInstance;							//インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;	        		//ウィンドウクラス名
	wc.lpfnWndProc = WndProc;							//ウィンドウプロシージャ(関数の名前だけ書く)
	wc.style = CS_VREDRAW | CS_HREDRAW;					//スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);			//アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);			//小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);			//マウスカーソル（白黒のものしか用意できない）
	wc.lpszMenuName = NULL;								//メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）
	RegisterClassEx(&wc);	//クラスを登録

	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);	//枠の表示に必要な大きさが返る



	//ウィンドウを作成
	HWND hWnd = CreateWindow(		//hWndはウィンドウのハンドル、インスタンスハンドルと同じく番号が割り振られる
		WIN_CLASS_NAME,	       //ウィンドウクラス名
		"ゲーム",      //タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,	//スタイル（普通のウィンドウ）
		CW_USEDEFAULT,	      //表示位置左（おまかせ）
		CW_USEDEFAULT,	      //表示位置上（おまかせ）
		winRect.right - winRect.left,	                //ウィンドウ幅
		winRect.bottom - winRect.top,	                //ウィンドウ高さ
		NULL,	               //親ウインドウ（なし）
		NULL,	               //メニュー（なし）
		hInstance,	          //インスタンス（実体）
		NULL	                //パラメータ（なし）
	);
	assert(hWnd != NULL);	//エラー処理

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);

	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	//Direct3Dの準備(初期化)
	Direct3D::Initialize(hWnd);

	Input::Initialize(hWnd);

	pRootJob = new RootJob;
	assert(pRootJob != nullptr);
	pRootJob->Initialize();

	//メッセージループ(何か起きるのを待つ)
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));	//構造体内すべてを０で初期化
	while (msg.message != WM_QUIT)
	{
		//メッセージあり、ウィンドウ削除などの操作があった場合そっちを優先
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし、ゲームの処理に集中
		else
		{
			
			//入力情報の更新
			Input::Update();

			//ゲーム内の更新
			pRootJob->UpdateSub();

			//描画開始
			Direct3D::BeginDraw();

			//描画
			pRootJob->DrawSub();

			//描画終了
			Direct3D::EndDraw();

		}
	}

	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Input::Release();
	//Direct3Dの開放処理
	Direct3D::Release();
	Model::AllRelease();
	Image::AllRelease();
	

	return 0;

}

//ウィンドウプロシージャ(何かあった時に呼ばれる関数)
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)	//HRESULTもある
{
	switch (msg)	//msgはメッセージの略
	{
	case WM_DESTROY:		//WM_DESTROYはウィンドウが閉じられたらの意
		PostQuitMessage(0);	//プログラム終了
		return 0;	//自分で処理を書いたものは、戻り値０と書く
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);	//特に処理がないWMにデフォルト値を返してくれる
}