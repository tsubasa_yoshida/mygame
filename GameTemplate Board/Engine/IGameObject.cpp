#include "IGameObject.h"
#include "Global.h"
#include "Collider.h"
#include "Direct3D.h"




IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
}

IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")
{
}

IGameObject::IGameObject(IGameObject * parent, const std::string & name) :
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)),
	dead_(false), lowFlag_(false), pCollider_(nullptr), pPlaneCollider_(nullptr),
	pBoxCollider_(nullptr)
{
}


IGameObject::~IGameObject()
{
	SAFE_DELETE(pCollider_);
	SAFE_DELETE(pPlaneCollider_);
	SAFE_DELETE(pBoxCollider_);
}


void IGameObject::Transform()
{
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;

	//移動行列
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);	//移動行列
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));				//縦回転
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));				//横回転
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));				//奥回転
	//拡大・縮小は原点からの距離を小さくしたり大きくするため最後に掛け合わせるのはNG
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);				//大きさ

	//拡大・縮小、回転、移動の順
	localMatrix_ = matS * matRX * matRY * matRZ * matT;

	//親が居なかったら
	if (pParent_ == nullptr)
	{
		//世界行列に親のローカル行列を格納
		worldMatrix_ = localMatrix_;
	}
	//既に親が居たら
	else
	{
		//世界行列に子のローカルと親ワールド行列を掛け合わせたものを格納
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
}

void IGameObject::UpdateSub()
{
	Update();
	Transform();

	Collision(SceneManager::GetCurrentScene());

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	//イテレータを使用しリストの要素分ループ
	for (auto it = childList_.begin(); it != childList_.end();)
	{
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			//戻り値を返してあげる
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
	}

}

void IGameObject::DrawSub()
{
	Draw();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
		(*it)->CollisionDraw();
	}
}

void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

IGameObject * IGameObject::FindChildObject(const std::string & name)
{
	//子供がいなければ終了
	if (childList_.empty())
		return nullptr;

	//イテレータ
	auto it = childList_.begin();	//先頭
	auto end = childList_.end();	//末尾

	//子オブジェクトから探す
	while (it != end) {

		//同じ名前のオブジェクトを見つけたらそのオブジェクトを返す
		if ((*it)->GetObjectName() == name)
			return *it;

		//更に子供がいないかを探査
		IGameObject* obj = (*it)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}

		//次のオブジェクトへ
		it++;
	}

	//なくなればnullptrを返す
	return nullptr;
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}

void IGameObject::KillMe()
{
	dead_ = true;
}

void IGameObject::SetCollider(D3DXVECTOR3 & center, float radius)
{
	pCollider_ = new Collider(this, center, radius);
}

void IGameObject::SetPlaneCollider(D3DXVECTOR3 & center, D3DXVECTOR3 & Normal)
{
	pPlaneCollider_ = new Collider(this, center, Normal);
}

void IGameObject::SetBoxCollider(D3DXVECTOR3 & center, D3DXVECTOR3 & size)
{
	pBoxCollider_ = new Collider(this, center, size);
}

void IGameObject::Collision(IGameObject* targetObject)
{
	
	//当たり判定が付いているものがあれば
	if (targetObject != this &&
		this->pCollider_ != nullptr &&
		targetObject->pCollider_ != nullptr &&
		pCollider_->IsHit(*targetObject->pCollider_)
		)
	{
		this->OnCollision(targetObject, targetObject->pCollider_, pCollider_);
	}

	//平面コライダイテレータ
	//auto it = targetObject->pPlaneCollider_.begin();	//先頭
	//auto end = targetObject->pPlaneCollider_.end();	//末尾

	//平面と球体
	if (targetObject != this &&
		this->pCollider_ != nullptr &&
		targetObject->pPlaneCollider_ != nullptr &&
		pCollider_->CalcParticlePlane(*targetObject->pPlaneCollider_, *this->pCollider_)
		)
	{
		this->OnCollision(targetObject, targetObject->pPlaneCollider_, pCollider_);
	}
	
	//球体と箱
	if (targetObject != this &&
		this->pCollider_ != nullptr &&
		targetObject->pBoxCollider_ != nullptr &&
		pCollider_->IsHitBoxVsParticle(*targetObject->pBoxCollider_)
		)
	{
		this->OnCollision(targetObject, targetObject->pBoxCollider_, pCollider_);
	}

	//箱と箱
	if (targetObject != this &&
		this->pBoxCollider_ != nullptr &&
		targetObject->pBoxCollider_ != nullptr &&
		pBoxCollider_->IsHitBoxVsBox(*targetObject->pBoxCollider_)
		)
	{
		this->OnCollision(targetObject, targetObject->pBoxCollider_, pBoxCollider_);
	}

	//子供との当たり判定用ループ
	for (auto it = targetObject->childList_.begin(); it != targetObject->childList_.end(); it++)
	{
		Collision(*it);
	}
}

void IGameObject::CollisionDraw()
{
	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);	//ワイヤーフレーム
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);				//ライティングOFF
	Direct3D::pDevice->SetTexture(0, nullptr);								//テクスチャなし

	for (auto i = this->childList_.begin(); i != this->childList_.end(); i++)
	{
		if ((*i)->GetObjectName() == "Ball"  && (*i)->GetCollider() != nullptr)
		{
			(*i)->GetCollider()->Draw((*i)->GetCollider()->GetPosition(), (*i)->Getrotate());
		}
		else if ((*i)->GetObjectName() == "Wall" && (*i)->GetPlaneCollider() != nullptr)
		{
			(*i)->GetPlaneCollider()->Draw((*i)->GetPlaneCollider()->GetPosition(), (*i)->Getrotate());
		}
		else if ((*i)->GetObjectName() == "Ground" && (*i)->GetPlaneCollider() != nullptr)
		{
			(*i)->GetPlaneCollider()->Draw((*i)->GetPlaneCollider()->GetPosition(), (*i)->Getrotate());
		}
	}

	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, true);
}

