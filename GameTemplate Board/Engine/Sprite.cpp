#include "Sprite.h"
#include "Direct3D.h"



Sprite::Sprite() :
		pSprite_(nullptr), pTexture_(nullptr)
{
}


Sprite::~Sprite()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pSprite_);
}

void Sprite::Load(const char* fileName)		//名前が変わらないものの場合はconstを付与する
{
	//スプライト作成
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_);	//第1引数どこに出すのか
	assert(pSprite_ != nullptr);

	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, fileName,			//ファイル以外からも読み込める、exeに直接埋め込まれてる実行ファイルなど
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);
}

void Sprite::Draw(const D3DXMATRIX& matrix2)
{
	//行列をセット
	pSprite_->SetTransform(&matrix2);

	//ゲーム画面の描画 (2の乗数以外の大きさの画像サイズだと黒い部分が出るので注意)
	pSprite_->Begin(0);
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, D3DXCOLOR(1, 1, 1, 1));
	pSprite_->End();
}
