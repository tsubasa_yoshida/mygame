#include "SceneManager.h"
#include "../PlayScene.h"


//静的メンバ変数は実行時に起きるのでここで初期値を格納
SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_PLAY;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_PLAY;
IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{
	//初期のシーンを格納
	currentSceneID_ = SCENE_ID_PLAY;
	nextSceneID_ = SCENE_ID_PLAY;
	pCurrentScene_ = nullptr;
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	pCurrentScene_ = CreateGameObject<PlayScene>(this);
}

//更新
void SceneManager::Update()
{
	if (currentSceneID_ != nextSceneID_)
	{
		//現在シーンの子供を削除
		auto scene = childList_.begin();
		(*scene)->ReleaseSub();
		SAFE_DELETE(*scene);


		//戻り値をsceneに返す(リスト自体の削除)
		//scene = childList_.erase(scene);
		childList_.clear();
		

		//シーン切替処理
		switch (nextSceneID_)
		{
			case SCENE_ID_PLAY: pCurrentScene_ = CreateGameObject<PlayScene>(this); break;

		}
		//現在のシーンを次のシーンに更新
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
