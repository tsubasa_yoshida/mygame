#include "Direct3D.h"
	
//グローバル変数の初期化
LPDIRECT3D9	Direct3D::pD3d = nullptr;	   //Direct3Dオブジェクト
LPDIRECT3DDEVICE9	Direct3D::pDevice = nullptr;	//Direct3Dデバイスオブジェクト


void Direct3D::Initialize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);	//
	assert(pD3d != nullptr);	//エラー処理

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;	                //専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));	          //中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;
	d3dpp.BackBufferHeight = g.screenHeight;
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);
	assert(pDevice != nullptr);		//エラー処理

	//アルファブレンド
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);				//半透明表示の設定
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング又はシェーディング
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);	//使わない場合はfalse

	//ライトを設置
	D3DLIGHT9 lightState;							//LIGHT型構造体作成
	ZeroMemory(&lightState, sizeof(lightState));	//ZeroMemoryで中身をすべて初期化

	//ライト各色1〜0の間で赤青緑の値を設定
	lightState.Type = D3DLIGHT_DIRECTIONAL;			//平行光源(ディレクショナルライト)
	lightState.Direction = D3DXVECTOR3(0, -5, 10);	//ライトが当たる向き
	lightState.Diffuse.r = 1.0f;	//赤
	lightState.Diffuse.g = 1.0f;	//緑
	lightState.Diffuse.b = 1.0f;	//青
	lightState.Ambient.r = 1.0f;
	lightState.Ambient.g = 1.0f;
	lightState.Ambient.b = 1.0f;
	pDevice->SetLight(0, &lightState);	//ライトの作成(第一引数はライトの番号、ここを変更することで複数設定することが可能)

	pDevice->LightEnable(0, TRUE);		//ライトのスイッチをオン(TRUEにする)Enableがついているものは有効か、無効か、などの意味

	//アンビエントライト
	pDevice->SetRenderState(D3DRS_AMBIENT, 0x7fffffff);

	//カメラ
	D3DXMATRIX view, proj;
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 1000.0f);
	pDevice->SetTransform(D3DTS_VIEW, &view);
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

void Direct3D::BeginDraw()
{
	//画面をクリア(都度白画面で)
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(100, 100, 255), 1.0f, 0);

	//描画開始
	pDevice->BeginScene();
}

void Direct3D::EndDraw()
{
	//描画終了
	pDevice->EndScene();

	//スワップ
	pDevice->Present(NULL, NULL, NULL, NULL);	//バックバッファを利用して次の絵を用意して差し替えする処理
}

void Direct3D::Release()
{
	//開放処理(作ったときと逆順で)
	pDevice->Release();
	pD3d->Release();
}
