#define _CRT_SECURE_NO_WARNINGS

#include "Ball.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Collider.h"

#include <stdio.h>

#include "Wall.h"
#include "Ground.h"
#include "Text.h"

//コンストラクタ
Ball::Ball(IGameObject * parent)
	:IGameObject(parent, "Ball"), hModel_(-1), weight_(1.0f), radius_(1.0f), vel_(0,0,0), acc_(0,grav,0), accFlag_(true), isGround_(false)
{
}

//デストラクタ
Ball::~Ball()
{
}

//初期化
void Ball::Initialize()
{
	hModel_ = Model::Load("Data//Ball.fbx");

	position_.y = 12.0f;

	position_.z = -10.0f;

	//vel_.x = 0.2f;

	//vel_.z = 0.3f;

	SetCollider(position_, radius_);
	
	pCollider_->SetPos(position_);
}

//更新
void Ball::Update()
{

	vel_ += acc_;

	pCollider_->SetPrePos(position_);

	position_ += vel_;

	pCollider_->SetPos(position_);
	
}

//描画
void Ball::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Ball::Release()
{
}

//コリジョン
void Ball::OnCollision(IGameObject * pTarget, Collider * collTarget, Collider * collThis)
{
	if (pTarget->GetObjectName() ==  "Wall" || "Ground" )
	{
		//法線
		//反射ベクトル
		//ベクトルの正規化
		D3DXVECTOR3 normal_n;
		D3DXVECTOR3 reflection;
		D3DXVec3Normalize(&normal_n, &collTarget->GetNormal());
		D3DXVec3Normalize(&reflection, &(vel_ - 2.0f * D3DXVec3Dot(&vel_, &normal_n) * normal_n));

		//位置を更新
		position_ = pCollider_->GetPrePosition();

		//反射ベクトルを更新
		vel_ = reflection * D3DXVec3Length(&vel_);
	}

	if (pTarget->GetObjectName() == "Ball")
	{
		//ぶつかった相手(ボール)の受け取り
		Ball pTargetBall = pTarget;

		//質量の合計値
		//反発係数計算
		//衝突ベクトル
		//ベクトルの正規化
		//内積計算
		//定数ベクトル
		float totalwei = 1.0f + 1.0f;
		float refRate = (1 + 0.8f * 0.8f);
		D3DXVECTOR3 collVec = collTarget->GetPosition() - collThis->GetPosition();
		D3DXVec3Normalize(&collVec, &collVec);
		float dot = D3DXVec3Dot( &(vel_ - pTargetBall.GetVel()), &collVec);
		D3DXVECTOR3 ConstVec = refRate * dot / totalwei * collVec;

		//反射ベクトルを更新
		vel_ = -0.8f * ConstVec + vel_;
		pTargetBall.SetVel(0.8f * ConstVec + pTargetBall.GetVel());

		//位置を更新
		position_ = position_ + 1 / 60 * vel_;
		pTargetBall.SetPosition(pTargetBall.GetPosition() + 1 / 60 * pTargetBall.GetVel());
	}
}

