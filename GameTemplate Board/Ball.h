#pragma once
#include "Engine/IGameObject.h"
#include "Physic.h"

//重力加速度
const float grav = -9.8f / 60.0f;

#define IKD_EPSIRON 0.00001f // 誤差

class Text;

//ボールを管理するクラス
class Ball : public IGameObject
{
	int hModel_;	//モデル

	float weight_;	//球の重さ

	float radius_;	//球の半径

	D3DXVECTOR3 acc_;

	D3DXVECTOR3 vel_;

	bool accFlag_;

	bool isGround_;

public:
	//コンストラクタ
	Ball(IGameObject* parent);

	//デストラクタ
	~Ball();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(IGameObject* pTarget, Collider* collTarget, Collider* collThis)override;

	void SetVel(D3DXVECTOR3 vel) { vel_ = vel; }

	D3DXVECTOR3 GetVel() { return vel_; }

};