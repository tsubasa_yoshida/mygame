#include "Wall.h"
#include "Engine/Model.h"
#include "Engine/Collider.h"

//コンストラクタ
Wall::Wall(IGameObject * parent)
	:IGameObject(parent, "Wall"), normal_(0, 0, 0), wallNum_(-1)
{
	
}

//デストラクタ
Wall::~Wall()
{
}

//初期化
void Wall::Initialize()
{
	string filename[4] = {
		"Data//WallR.fbx",
		"Data//WallL.fbx",
		"Data//WallB.fbx",
		"Data//WallF.fbx",
	};


	for (int i = 0; i < WALL_MAX; i++)
	{
		hModel_[i] = Model::Load(filename[i].c_str());
	}

	SetPlaneCollider(position_, normal_);

	pPlaneCollider_->SetPos(position_);
}

//更新
void Wall::Update()
{
	pPlaneCollider_->SetNormal(normal_);

	pPlaneCollider_->SetPos(position_);
}

//描画
void Wall::Draw()
{
	for (int i = 0; i < WALL_MAX; i++)
	{
		Model::SetMatrix(hModel_[wallNum_], worldMatrix_);
		Model::Draw(hModel_[wallNum_]);
	}
}

//開放
void Wall::Release()
{
}

void Wall::SetNormal(D3DXVECTOR3 normal)
{
	normal_ = normal;
}

void Wall::SetNumber(int number)
{
	wallNum_ = number;
}

D3DXVECTOR3 Wall::GetNormal()
{
	return normal_;
	//return D3DXVECTOR3();
}
