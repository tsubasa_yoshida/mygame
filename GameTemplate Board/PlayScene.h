#pragma once

#include "Engine/global.h"

#include <string>

using namespace std;

class Wall;

class Camera;

class Ground;


//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	int hModel_;

	string str;

	Wall* pWall_[4];

	Camera* pCamera;

	Ground* pGround_;

	D3DXVECTOR3 camPos;
	
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
