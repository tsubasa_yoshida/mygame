#include "PlayScene.h"
#include "Engine/Model.h"

#include "Engine/Camera.h"
#include "Ball.h"
#include "Ground.h"
#include "Wall.h"
#include "Engine/Input.h"

D3DXVECTOR3 WallPos[4] = {
	D3DXVECTOR3(30, 0, 0),
	D3DXVECTOR3(-30, 0, 0),
	D3DXVECTOR3(0, 0, -30),
	D3DXVECTOR3(0, 0, 30),
};

D3DXVECTOR3 NormalBuf[4] = {
	D3DXVECTOR3(-1, 0, 0),	//�E��
	D3DXVECTOR3(1, 0, 0),	//����
	D3DXVECTOR3(0, 0, 1),	//��O����
	D3DXVECTOR3(0, 0, -1)	//������
};

//�R���X�g���N�^
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")
{
}

//������
void PlayScene::Initialize()
{
	pGround_ = CreateGameObject<Ground>(this);
	
	for (int i = 0; i < 4; i++)
	{
		pWall_[i] = CreateGameObject<Wall>(this);
		pWall_[i]->SetPosition(WallPos[i]);
		pWall_[i]->SetNormal(NormalBuf[i]);
		pWall_[i]->SetNumber(i);
	}

	camPos = D3DXVECTOR3(0, 10, -40);

	pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(camPos);
	pCamera->SetTarget(D3DXVECTOR3(0, 0, 0));
}

//�X�V
void PlayScene::Update()
{
	D3DXMATRIX mat;

	if (Input::IsKeyDown(DIK_SPACE))
	{
		CreateGameObject<Ball>(this);
	}

	if (Input::IsKey(DIK_RIGHTARROW))
	{
		//camPos.x += 0.2f;
		//D3DXMatrixRotationZ(&mat, D3DXToRadian(45));
		//pGround_->SetRotate(mat);
	}

	if (Input::IsKey(DIK_LEFTARROW))
	{
		//camPos.x -= 0.2f;
	}

	if (Input::IsKey(DIK_UPARROW))
	{
		//camPos.y += 0.2f;
	}

	if (Input::IsKey(DIK_DOWNARROW))
	{
		//camPos.y -= 0.2f;
	}

	pCamera->SetPosition(camPos);
}

//�`��
void PlayScene::Draw()
{
	
}

//�J��
void PlayScene::Release()
{
	for (int i = 0; i < 4; i++)
	{
		SAFE_RELEASE(pWall_[i]);
	}
}