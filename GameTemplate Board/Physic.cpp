#include "Physic.h"



Physic::Physic(): pow_(D3DXVECTOR3(0, 0, 0)), acc_(D3DXVECTOR3(0, 0, 0)), vel_(D3DXVECTOR3(0, 0, 0))
{
}


Physic::~Physic()
{
}

D3DXVECTOR3 Physic::Velocity(D3DXVECTOR3 acc)
{
	return vel_ += acc_;
}

D3DXVECTOR3 Physic::Acceleration(D3DXVECTOR3 pow_, float weight)
{
	return acc_ = pow_ / weight;
}

void Physic::SetAcc(D3DXVECTOR3 acc)
{
	acc_ = acc;
}

void Physic::SetVel(D3DXVECTOR3 vel)
{
	vel_ = vel;
}

void Physic::SetPow(D3DXVECTOR3 pow)
{
	pow_ = pow;
}

D3DXVECTOR3 Physic::GetAcc()
{
	return acc_;
}

D3DXVECTOR3 Physic::GetVel()
{
	return vel_;
}

D3DXVECTOR3 Physic::GetPow()
{
	return pow_;
}
