#pragma once
#include "Engine/IGameObject.h"
#include <vector>


//地面を管理するクラス
class Ground : public IGameObject
{

	int hModel_;

	D3DXVECTOR3 normal_;

	int count = 0;
	
public:
	//コンストラクタ
	Ground(IGameObject* parent);

	//デストラクタ
	~Ground();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	void SetRotate(D3DXMATRIX rotateVec);

	
};