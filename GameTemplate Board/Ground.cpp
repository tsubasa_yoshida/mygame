#include "Ground.h"
#include "Engine/Model.h"
#include "Ball.h"
#include "Engine/Collider.h"

//コンストラクタ
Ground::Ground(IGameObject * parent)
	:IGameObject(parent, "Ground"), hModel_(-1)
{
	SetPlaneCollider(position_, D3DXVECTOR3(0, 1, 0));
	pPlaneCollider_->SetPos(position_);
}

//デストラクタ
Ground::~Ground()
{
}

//初期化
void Ground::Initialize()
{
	position_ = D3DXVECTOR3(0, 0, 0);

	SetPlaneCollider(position_, D3DXVECTOR3(0, 1, 0));

	position_.y = 0;

	hModel_ = Model::Load("Data//Ground.fbx");

	pPlaneCollider_->SetPos(position_);
}

//更新
void Ground::Update()
{
}

//描画
void Ground::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Ground::Release()
{
}

void Ground::SetRotate(D3DXMATRIX rotateVec)
{
	D3DXVECTOR3 vec = D3DXVECTOR3(0, 0, -1);
	D3DXVec3TransformCoord(&rotate_, &vec, &rotateVec);
}

