#pragma once
#include "Engine/IGameObject.h"
#include <vector>

#define MAP_HEIGHT 15
#define MAP_WIDTH  15
#define HISTORY_MAX 100


//地面を管理するクラス
class Ground : public IGameObject
{
	struct
	{
		int height;
		int type;
	} table_[MAP_HEIGHT][MAP_WIDTH];

	int hModel_;

	D3DXVECTOR3 normal_;
	
public:
	//コンストラクタ
	Ground(IGameObject* parent);

	//デストラクタ
	~Ground();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ゲッター
	int GetModelNumber() { return hModel_; };
	
};