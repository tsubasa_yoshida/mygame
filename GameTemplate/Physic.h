#pragma once

#include "Engine/Global.h"

//物理計算を管理するクラス
class Physic
{
private:

	//加速度
	D3DXVECTOR3 acc_;

	//速度
	D3DXVECTOR3 vel_;

	//力
	D3DXVECTOR3 pow_;



public:
	Physic();
	~Physic();

	//速度計算
	D3DXVECTOR3 Velocity(D3DXVECTOR3 acc);

	//加速度計算
	D3DXVECTOR3 Acceleration(D3DXVECTOR3 pow_, float weight);

	//加速度設定
	void SetAcc(D3DXVECTOR3 acc);

	//速度設定
	void SetVel(D3DXVECTOR3 vel);

	//力設定
	void SetPow(D3DXVECTOR3 pow);

	//加速度受け渡し
	D3DXVECTOR3 GetAcc();

	//速度受け渡し
	D3DXVECTOR3 GetVel();

	//力受け渡し
	D3DXVECTOR3 GetPow();

};

