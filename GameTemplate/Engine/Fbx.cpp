#include "Fbx.h"
#include "Direct3D.h"

Fbx::Fbx(LPD3DXEFFECT pEffect) :
	pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), pTexture_(nullptr), pMaterial_(nullptr),
	pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr), vertexCount_(0), indexCount_(0), polygonCount_(0), materialCount_(0),
	polygonCountOfMaterial_(nullptr), pEffect_(pEffect)
{
	RayCastData();
}


Fbx::~Fbx()
{
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	SAFE_DELETE_ARRAY(pMaterial_);

	//ダブルポインタの開放
	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_RELEASE(pTexture_[i]);
	}
	SAFE_DELETE_ARRAY(ppIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);

	SAFE_RELEASE(pVertexBuffer_);
	pScene_->Destroy();
	pManager_->Destroy();
}

void Fbx::Load(const char * fileName)
{
	pManager_ = FbxManager::Create();
	assert(pManager_ != nullptr);
	pImporter_ = FbxImporter::Create(pManager_, "");
	assert(pImporter_ != nullptr);
	pScene_ = FbxScene::Create(pManager_, "");
	assert(pScene_ != nullptr);
	//ファイルを読み込む
	pImporter_->Initialize(fileName);
	pImporter_->Import(pScene_);

	//開いた時点でImporterは必要なくなるので開放処理
	pImporter_->Destroy();

	//デフォルト(現在)のカレントディレクトリ位置を取得、格納
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//カレントディレクトリの変更用の処理
	char dir[MAX_PATH];
	_splitpath_s(fileName, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);

	//ルートノードへアクセスする(取得)
	FbxNode* rootNode = pScene_->GetRootNode();
	assert(rootNode != nullptr);

	//ルートノードの子が何人かを調べる
	int childCount = rootNode->GetChildCount();

	//上記で求めた子の数分ループ
	for (int i = 0; childCount > i; i++)
	{
		//ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
	}

	//デフォルトディレクトリに戻す
	SetCurrentDirectory(defaultCurrentDir);
}

//Fbxのノードをチェックする関数
void Fbx::CheckNode(FbxNode * pNode)
{
	//Fbxの情報を受け取る
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	assert(attr != nullptr);

	//メッシュかどうかの判定
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		//マテリアルの数を取得
		materialCount_ = pNode->GetMaterialCount();
		//マテリアルの大きさ分の領域を確保
		pMaterial_ = new D3DMATERIAL9[materialCount_];
		assert(pMaterial_ != nullptr);

		pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];
		assert(pTexture_ != nullptr);

		//面倒な初期化ループ処理
		/*
		for (int i = 0; i < materialCount_; i++)
		{
			pTexture_[i] = nullptr;
		}
		*/

		//Windowsでのみ扱える初期化処理
		ZeroMemory(pTexture_, sizeof(LPDIRECT3DTEXTURE9) * materialCount_);
		

		//見つけたマテリアル分ループ
		for (int i = 0; i < materialCount_; i++)
		{
			//シンプルなものを出すためにとりあえずPhong
			FbxSurfacePhong* surface = (FbxSurfacePhong*)pNode->GetMaterial(i);
			assert(surface != nullptr);

			FbxDouble3 diffuse = surface->Diffuse;
			FbxDouble3 ambient = surface->Ambient;
			FbxDouble3 specular;

			ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

			pMaterial_[i].Diffuse.r = (float)diffuse[0];
			pMaterial_[i].Diffuse.g = (float)diffuse[1];
			pMaterial_[i].Diffuse.b = (float)diffuse[2];
			pMaterial_[i].Diffuse.a = 1.0f;					//透明度の指定　基本的にプログラム側ではいじらない

			pMaterial_[i].Ambient.r = (float)ambient[0];
			pMaterial_[i].Ambient.g = (float)ambient[1];
			pMaterial_[i].Ambient.b = (float)ambient[2];
			pMaterial_[i].Ambient.a = 1.0f;

			if (surface->GetClassId().Is(FbxSurfacePhong::ClassId))
			{
				//鏡面反射光
				FbxDouble3 specular = surface->Specular;
				pMaterial_[i].Specular.r = (float)specular[0];
				pMaterial_[i].Specular.g = (float)specular[1];
				pMaterial_[i].Specular.b = (float)specular[2];
				pMaterial_[i].Specular.a = 1.0f;

				pMaterial_[i].Power = (float)surface->Shininess;
			}


			//テクスチャを使っているかの検索
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			if (textureFile == nullptr)
			{
				pTexture_[i] = nullptr;
			}
			else
			{
				const char* textureFileName = textureFile->GetFileName();

				char name[_MAX_FNAME];
				char ext[_MAX_EXT];

				_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				//wsprintfは第一引数に以降引数を繋げたものが格納される
				//char以外の型の文字列も格納可能
				wsprintf(name, "%s%s", name, ext);

				D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN,
					D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pTexture_[i]);
				//ポインタの中身が（ 0xcdcdcdc...etc ）になっている場合はデータの読み込みができていない状態
				//今扱っているものはダブルポインタなので先頭のみの初期化しか行っていないため中身が不定になっている
				//中身を初期化する処理が必要
				assert(pTexture_[i] != nullptr);
			}
		}

		CheckMesh(pNode->GetMesh());
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			CheckNode(pNode->GetChild(i));
		}
	}
}

//メッシュ情報をチェックする関数
void Fbx::CheckMesh(FbxMesh* pMesh)
{
	//頂点情報受け取り(二次元配列で)
	FbxVector4* pVertexPos = pMesh->GetControlPoints();

	//頂点数代入
	vertexCount_ = pMesh->GetControlPointsCount();

	//vertexCount_分の大きさの配列を確保
	Vertex* vertexList = new Vertex[vertexCount_];
	assert(vertexList != nullptr);

	//ポリゴン数代入
	polygonCount_ = pMesh->GetPolygonCount();

	//インデックス数代入
	indexCount_ = pMesh->GetPolygonVertexCount();
	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	//法線をポリゴンごとに読み込む為のループ
	for (int i = 0; i < polygonCount_; i++)
	{

		int startIndex = pMesh->GetPolygonVertexIndex(i);
		
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			//Normalに法線の情報を格納
			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			//UVの情報をuvに格納、向きが逆になっているので１引いている
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);
	
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	pVertexBuffer_->Unlock();

	//newで作成した配列を開放
	SAFE_DELETE_ARRAY(vertexList);

	//インデックス分の大きさの配列を確保
	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];
	assert(ppIndexBuffer_ != nullptr);

	//マテリアルの数の大きさ分領域を確保
	polygonCountOfMaterial_ = new int[materialCount_];
	assert(polygonCountOfMaterial_ != nullptr);


	for (int i = 0; i < materialCount_; i++)
	{
		int* indexList = new int[indexCount_];
		assert(indexList != nullptr);
		int count = 0;
		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//ポリゴンがどのマテリアルを使っているかを取得
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				//マテリアルが一致したときインデックスバッファに格納
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}

		//マテリアルごとのポリゴン数を格納
		polygonCountOfMaterial_[i] = count / 3;

		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		assert(ppIndexBuffer_[i] != nullptr);

		DWORD *iCopy;


		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();
		SAFE_DELETE_ARRAY(indexList);
	}

}

void Fbx::Draw(const D3DXMATRIX & matrix)
{
	//頂点バッファの設定
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Fbx::Vertex));

	//頂点ストリームを指定
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	//ワールド行列をセット（パーツごとの行列を先にかける）
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	//マテリアルごとに描画
	for (int i = 0; i < materialCount_; i++)
	{
		Direct3D::pDevice->SetIndices(ppIndexBuffer_[i]);
		Direct3D::pDevice->SetTexture(0, pTexture_[i]);
		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);

		if (pEffect_)
		{
			//ディフューズ設定
			//Torusクラスから受け取ってきたpEffect_でMayaで使用したマテリアルの色を設定
			pEffect_->SetVector("DIFFUSE_COLOR",
				&D3DXVECTOR4(pMaterial_[i].Diffuse.r, pMaterial_[i].Diffuse.g, pMaterial_[i].Diffuse.b, pMaterial_[i].Diffuse.a));

			//環境光
			pEffect_->SetVector("AMBIENT_COLOR",
				&D3DXVECTOR4(pMaterial_[i].Ambient.r, pMaterial_[i].Ambient.g, pMaterial_[i].Ambient.b, pMaterial_[i].Ambient.a));

			pEffect_->SetVector("SPECULER_COLOR",
				&D3DXVECTOR4(
					pMaterial_[i].Specular.r,
					pMaterial_[i].Specular.g,
					pMaterial_[i].Specular.b,
					pMaterial_[i].Specular.a));

			pEffect_->SetFloat("SPECULER_POWER",
				pMaterial_[i].Power);

			pEffect_->SetBool("IS_TEXTURE", pTexture_[i] != nullptr);

		}
		else
		{
			Direct3D::pDevice->SetMaterial(&pMaterial_[i]);
		}
	}
}

void Fbx::RayCast(RayCastData& data, const D3DXMATRIX& matrix)
{
	D3DXVec3Normalize(&data.direction, &data.direction);

	//頂点バッファをロック
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	float preDistance = 9999.0f;

	//衝突点を求める際に使用
	float u, v;

	//マテリアル毎
	for (DWORD i = 0; i < (DWORD)materialCount_; i++)
	{
		//インデックスバッファをロック
		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);

		//そのマテリアルのポリゴン毎
		for (DWORD j = 0; j < (DWORD)polygonCountOfMaterial_[i]; j++)
		{
			//3頂点
			//この時点ではローカル座標
			D3DXVECTOR3 v0, v1, v2;
			v0 = vCopy[iCopy[j * 3 + 0]].pos;
			v1 = vCopy[iCopy[j * 3 + 1]].pos;
			v2 = vCopy[iCopy[j * 3 + 2]].pos;

			//ローカル行列にワールド行列をかけている
			//本当は重くなるから、実際にサイコロに使った逆行列をレイの向きに使ってやると、
			//処理が速くなる？レイ自体を回転させる
			D3DXVec3TransformCoord(&v0, &v0, &matrix);
			D3DXVec3TransformCoord(&v1, &v1, &matrix);
			D3DXVec3TransformCoord(&v2, &v2, &matrix);



			if (data.distance < preDistance)
			{

				BOOL isHit = D3DXIntersectTri(&v0, &v1, &v2, &data.start,
					&data.direction, &u, &v, &data.distance);

				//float u, v;     //例の衝突点を求めるのに使う値（いらないならnullptrでOK）
				//u,vは衝突したときその地点でエフェクトを追加したりしない限りnullptr
				// ※ここで、D3DXIntersectTri関数を使う
				if (isHit)
				{
					//レイの距離の更新
					preDistance = data.distance;

					data.hit = true;


					//ポリゴンの頂点から頂点に伸びるベクトルから外積を使い法線を求める
					D3DXVec3Cross(&data.normal, &(v0 - v1), &(v0 - v2));
					//上と下の面の法線がおなじ
					//data.normal.y *= -1;	←正常に跳ね変える（逆）
					//data.normal.y = 0.0f; ←跳ね返らない

					//衝突位置を検知
					D3DXVec3BaryCentric(&data.collisionPoint, &v0, &v1, &v2, u, v);

					//ローカル座標からワールド座標に変換
					//D3DXVec3TransformCoord(&data.collisionPoint, &data.collisionPoint, &(matrix));

				}
			}
		}

		//レイの発射位置 ー レイの着弾位置 = 向きが求められる

		//インデックスバッファ使用終了
		ppIndexBuffer_[i]->Unlock();
	}
	data.distance = preDistance;

}

