#pragma once
#include <fbxsdk.h>
#include "Global.h"
#include "Direct3D.h"

#pragma comment(lib,"libfbxsdk-mt.lib")

//レイキャスト
struct RayCastData
{
	D3DXVECTOR3 start;		//レイが発射される位置
	D3DXVECTOR3 direction;	//レイが発射される方向
	bool hit;				//レイが当たったか
	float distance;			//レイが当たった点までの距離,length
	D3DXVECTOR3 normal;		//当たったポリゴンの法線
	D3DXVECTOR3 collisionPoint; //衝突点



	RayCastData()
	{
		start = D3DXVECTOR3(0, 0, 0);
		direction = D3DXVECTOR3(0, 0, 0);
		hit = false;
		distance = 0.0f;
		normal = D3DXVECTOR3(0, 0, 0);
		collisionPoint = D3DXVECTOR3(0, 0, 0);

	}
};

class Fbx
{

	struct Vertex				//定義順に並べる
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;			//uvなので二次元
	};

	FbxManager*  pManager_;
	FbxImporter* pImporter_;
	FbxScene*    pScene_;

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9* ppIndexBuffer_;	//マテリアルが複数あるため配列用にポインタ

	LPDIRECT3DTEXTURE9* pTexture_;	//テクスチャ
	D3DMATERIAL9*       pMaterial_;	//マテリアル( 複数使うために配列を扱うためポインタ )

	LPD3DXEFFECT pEffect_;

	void CheckNode(FbxNode* pNode);
	void CheckMesh(FbxMesh* pMesh);

public:
	Fbx(LPD3DXEFFECT pEffect_);
	~Fbx();

	Vertex vertexList;

	int vertexCount_;	//頂点数
	int polygonCount_;	//ポリゴン数
	int indexCount_;	//インデックス数
	int materialCount_; //マテリアル数
	int* polygonCountOfMaterial_; //マテリアル毎のポリゴン数


	void Load(const char* fileName);
	void Draw(const D3DXMATRIX& matrix);

	FbxImporter* GetImporter() { return pImporter_; }

	//レイキャスト
	void RayCast(RayCastData& data, const D3DXMATRIX& matrix);
};