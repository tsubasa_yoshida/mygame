#include "Model.h"
#include<vector>

namespace Model
{
	std::vector<ModelData*> dataList;


	int Load(std::string filename, LPD3DXEFFECT pEffect)
	{
		ModelData* pData = new ModelData;
		pData->fileName = filename;

		bool isExist = false;

		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//今まで読み込んだモデルと今回読み込んだモデルの名前が同じかチェック
			if (dataList[i]->fileName == filename)
			{
				//アドレスをコピー
				pData->pFbx = dataList[i]->pFbx;
				isExist = true;
			}
		}

		if (isExist == false)
		{
			//新しいモデルならそのモデルを読み込み
			pData->pFbx = new Fbx(pEffect);

			pData->pFbx->Load(filename.c_str());
		}



		//モデルデータを格納
		dataList.push_back(pData);

		//帰ってくる要素の値が１よりも０のほうが管理しやすいのでー１
		return dataList.size() - 1;
	}

	void Draw(int handle)
	{
		//データリストに格納されたfbxデータのドローを呼び出す
		dataList[handle]->pFbx->Draw(dataList[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		dataList[handle]->matrix = matrix;
	}

	void Release(int handle)
	{

		bool isExist = false;

		//ハンドル番目のおんなじFBXがないかチェっく同じのがあったら消さない
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//handleはLoad関数の戻り値なのでDataListの場所と紐付けられてるので自分という証明ができる
			//自分は消さないのでスキップ
			if (i == handle)
			{
				continue;
			}

			//同じFBXだったら消さない
			//そこにデータがあって　かつ　同じFBXだったら
			if (dataList[i] != nullptr && dataList[i]->pFbx == dataList[handle]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		//同じじゃなかったら消す
		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pFbx);
		}
		SAFE_DELETE(dataList[handle]);


	}

	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//消してなかったら
			if (dataList[i] != nullptr)
			{
				Release(i);
			}

			SAFE_DELETE(dataList[i]);
		}
		dataList.clear();
	}


	void RayCast(int handle, RayCastData& data)
	{
		dataList[handle]->pFbx->RayCast(data,
			dataList[handle]->matrix);
	}
};
