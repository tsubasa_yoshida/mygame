#pragma once
#include "IGameObject.h"

//シーン管理用
enum SCENE_ID
{
	SCENE_ID_PLAY,
};

//シーンマネージャを管理するクラス
//シーンの切り替えを行う為
//そのシーンの処理が１フレーム分全て終わってから行う
class SceneManager : public IGameObject
{
	//今のシーンを指す静的メンバ変数
	static SCENE_ID currentSceneID_;
	//次のシーンを指す静的メンバ変数
	static SCENE_ID nextSceneID_;
	//
	static IGameObject* pCurrentScene_;
	

public:
	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//次のシーン受け取り用静的関数
	static void ChangeScene(SCENE_ID next);

	static IGameObject* GetCurrentScene() { return pCurrentScene_; }

};
