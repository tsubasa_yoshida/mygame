#include "Image.h"
#include <vector>

//CPP
namespace Image
{
	std::vector<ImageData*> dataList;

	//画像をロード
	int Load(std::string fileName)
	{
		ImageData* pData = new ImageData;
		assert(pData != nullptr);
		pData->fileName = fileName;

		//開いたファイル一覧から同じファイル名のものが無いか探す
		bool isExist = false;
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i]->fileName == fileName)
			{
				pData->pSprite = dataList[i]->pSprite;
				isExist = true;
				break;
			}
		}

		//開いたことのないファイルだった時
		if (isExist == false)
		{
			//新しいファイルを読み込み
			pData->pSprite = new Sprite;
			assert(pData != nullptr);
			pData->pSprite->Load(fileName.c_str());
		}

		dataList.push_back(pData);

		return dataList.size() - 1;
	}

	//描画
	void Draw(int handle)
	{
		dataList[handle]->pSprite->Draw(dataList[handle]->matrix);
	}

	//位置を格納
	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		dataList[handle]->matrix = matrix;
	}

	//開放処理
	void Release(int handle)
	{
		bool isExist = false;
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (i != handle && dataList[i] != nullptr &&
				dataList[i]->pSprite == dataList[handle]->pSprite)
			{
				isExist = true;
				break;
			}
		}

		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pSprite);
		}
		SAFE_DELETE(dataList[handle]);
	}

	//全てのオブジェクトの開放
	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i] != nullptr)
			{
				Release(i);
			}
		}
		dataList.clear();
	}

};