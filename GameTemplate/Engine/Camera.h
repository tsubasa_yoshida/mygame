#pragma once
#include "IGameObject.h"

//カメラを管理するクラス
class Camera : public IGameObject
{
	//カメラの見る位置
	D3DXVECTOR3 target_;


public:
	//コンストラクタ
	Camera(IGameObject* parent);

	//デストラクタ
	~Camera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void SetTarget(D3DXVECTOR3 target) { target_ = target; }
};