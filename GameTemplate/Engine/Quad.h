#pragma once
#include "Global.h"
#include "Direct3D.h"


class Quad
{
protected:
	struct Vertex				//定義順に並べる
	{
		D3DXVECTOR3 pos;
		//DWORD       color;	//符号なし整数型で色の構造体
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;			//uvなので二次元
	};

	LPDIRECT3DTEXTURE9 pTexture_;	//テクスチャ
	D3DMATERIAL9       material_;	//マテリアル

public:
	Quad();
	~Quad();

	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;
	Vertex vertexList;

	int vertexCount_;	//頂点数
	int polygonCount_;	//ポリゴン数

	
	void TextureLoad(const char* fileName);
	virtual void IndexLoad();
	virtual void VertexLoad();

    virtual void Load(const char* fileName);
	virtual void Draw(const D3DXMATRIX& matrix);
};