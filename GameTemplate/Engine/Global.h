#pragma once
//グルーバルヘッダはすべてのヘッダにインクルードされているので全体で扱いたいヘッダはここに記述
#include <d3dx9.h>		//このヘッダと下記リンカを記述することでDirect3Dが扱えるようになる
#include <assert.h>		//エラー処理、実行エラーが起きる可能性がある場合記述する(Create系)
#include "IGameObject.h"
#include "Input.h"
#include "SceneManager.h"

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}		//ポインタの開放用マクロ
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete[] p; p = nullptr;} //配列開放用マクロ
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}	//Releaseを用いた開放用マクロ


struct Global
{
	int screenWidth;
	int screenHeight;
};
extern Global g;

//extern const int	WINDOW_WIDTH = 800;	 //ウィンドウの幅
//extern const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ

