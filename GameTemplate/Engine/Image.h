#include <string>
#include "Sprite.h"
//H
namespace Image
{
	struct ImageData
	{
		std::string fileName;
		Sprite*	pSprite;
		D3DXMATRIX matrix;

		ImageData() : fileName(""), pSprite(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string fileName);
	void Draw(int handle);
	void SetMatrix(int handle, D3DXMATRIX& matrix);
	void Release(int handle);
	void AllRelease();
};