#include "Quad.h"



Quad::Quad() :
	pVertexBuffer_(nullptr), pIndexBuffer_(nullptr), pTexture_(nullptr), material_({ 0 })	//マテリアルはただの構造体、0で初期化が可能
{
}
	

Quad::~Quad()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}


void Quad::TextureLoad(const char* fileName)
{
	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, fileName,			//ファイル以外からも読み込める、exeに直接埋め込まれてる実行ファイルなど
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);
}

void Quad::IndexLoad()
{
	//四角形の頂点情報だと多くのパターンが考えられてしまうので二つの三角形に分割
	//ポリゴンには裏表があるので頂点の羅列の順番は時計回りになるように
	//0,2,3の頂点を結んだ三角形と0.1.2を結んだ三角形を配列に羅列
	//インデックス情報
	int indexList[] = { 0, 2, 3, 0, 1, 2};

	//インデックスバッファ格納処理　※処理内容はバーテックスと同様
	Direct3D::pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32,
		D3DPOOL_MANAGED, &pIndexBuffer_, 0);
	assert(pIndexBuffer_ != nullptr);
	DWORD *iCopy;
	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	pIndexBuffer_->Unlock();

	//ポリゴン数を算出
	polygonCount_ = sizeof(indexList) / sizeof(int) / 3;

}

void Quad::VertexLoad()
{
	//要素を省略して配列の宣言をすると宣言された分の大きさがとられる
	//D3DXVECTOR3が座標位置,D3DXCOLORが色
	//頂点情報
	Vertex vertexList[] = {
		   D3DXVECTOR3(-1, 1, 0), D3DXVECTOR3(0, 0, -1),  D3DXVECTOR2(0, 0),//D3DXCOLOR(255, 255, 255, 0),	//白透明
		   D3DXVECTOR3(1, 1, 0),  D3DXVECTOR3(0, 0, -1),  D3DXVECTOR2(1, 0),//D3DXCOLOR(255, 0, 255, 255),	//マゼンダ
		   D3DXVECTOR3(1, -1, 0), D3DXVECTOR3(0, 0, -1),  D3DXVECTOR2(1, 1),//D3DXCOLOR(0, 255, 0, 255),	//緑
		   D3DXVECTOR3(-1, -1, 0),D3DXVECTOR3(0, 0, -1),  D3DXVECTOR2(0, 1),//D3DXCOLOR(255, 0, 0, 255),	//赤
	};

	//バーテックスバッファ格納処理
	//D3DFVF_XYZなどはdefineされている順番で記述する点に注意
	Direct3D::pDevice->CreateVertexBuffer(sizeof(vertexList), 0,
		D3DFVF_XYZ /*| D3DFVF_DIFFUSE,*/ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &pVertexBuffer_, 0);	//DIFFUSEは拡散反射光
	assert(pVertexBuffer_ != nullptr);	//エラー処理

	Vertex *vCopy;									//Vertexコピー用ポインタ作成
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);	//内容をコピーしデータをロック
	memcpy(vCopy, vertexList, sizeof(vertexList));	//第一引数に第二引数を第三引数データの大きさ分コピー
	pVertexBuffer_->Unlock();						//コピー終了後、ロック解除

	//頂点数を算出
	vertexCount_ = sizeof(vertexList) / sizeof(Vertex);
}

void Quad::Load(const char* fileName)
{

	VertexLoad();

	TextureLoad(fileName);

	IndexLoad();


	//マテリアルの設定ポリゴンの反射する光の量(RGB)
	material_.Diffuse.r = 1.0f;
	material_.Diffuse.g = 1.0f;
	material_.Diffuse.b = 1.0f;

	//光が当たっていなくても20%光を反射させる
	material_.Ambient.r = 0.2f;
	material_.Ambient.g = 0.2f;
	material_.Ambient.b = 0.2f;

}

void Quad::Draw(const D3DXMATRIX& matrix)
{
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);
	Direct3D::pDevice->SetTexture(0, pTexture_);	//使いたいテクスチャの指定(第一引数が番号の指定)
	Direct3D::pDevice->SetMaterial(&material_);		//マテリアルの指定
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex)); //表示したい情報が入ってる場所を指定
	Direct3D::pDevice->SetIndices(pIndexBuffer_);
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ /*| D3DFVF_DIFFUSE*/ | D3DFVF_NORMAL | D3DFVF_TEX1);	//一頂点当たりの情報(論理和)
	Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCount_);	//第一引数　第四引数は頂点数　第六引数は出したいものの数

}

	