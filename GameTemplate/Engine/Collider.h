#pragma once
#include "Global.h"
class Collider
{
	//当たり判定の中心位置
	D3DXVECTOR3 center_;

	//法線記憶用
	D3DXVECTOR3 normal_;

	//箱の大きさ記憶用
	D3DXVECTOR3 size_;

	//半径
	float      radius_;

	//誰のオブジェクトか判別用メンバ変数
	IGameObject* owner_;

	FLOAT r_;

	D3DXVECTOR3 out_coll_;

	//テスト表示用の枠
	LPD3DXMESH	pMesh_;

public:
	Collider(IGameObject* owner, D3DXVECTOR3 center, float radius);
	Collider(IGameObject* owner, D3DXVECTOR3 center, D3DXVECTOR3 normal);
	//Collider(IGameObject* owner, D3DXVECTOR3 center, D3DXVECTOR3 size);
	~Collider();

	void SetPos(D3DXVECTOR3 pos);

	void SetNormal(D3DXVECTOR3 normal);

	D3DXVECTOR3 GetNormal();

	D3DXVECTOR3 GetPosition() { return center_; }

	bool IsHit(Collider& target);

	bool CalcParticlePlane(Collider& plane , Collider& particle);

	bool IsHitBoxVsParticle(Collider& target);

	bool IsHitBoxVsBox(Collider& target);

	bool IsHitBoxVsParticleOBB(Collider& target);

	//テスト表示用の枠を描画
	//引数	:	表示位置、回転
	//戻り値	:	なし
	void Draw(D3DXVECTOR3 position, D3DXVECTOR3 rotate);

	

};

