#pragma once
#include <string>
#include "Fbx.h"

namespace Model
{
	struct ModelData
	{
		std::string fileName;

		Fbx* pFbx;

		D3DXMATRIX matrix;

		//コンストラクタ
		ModelData() : fileName(""), pFbx(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	//モデルのロード
	//引数:モデルのファイル名
	//戻り値:モデルのハンドル
	int Load(std::string filename, LPD3DXEFFECT pEffect = nullptr);

	//モデルの描画
	//引数:モデルのハンドル
	//戻り値:なし
	void Draw(int handle);

	//モデルの変形するための情報を受け取る
	//引数: モデルのハンドル、行列
	//戻り値:なし
	void SetMatrix(int handle, D3DXMATRIX& matrix);

	//解放処理
	//引数:開放するモデルのハンドル
	//戻り値: なし
	void Release(int handle);

	//すべてを開放
	//引数:なし
	//戻り値:なし
	void AllRelease();

	//レイキャスト
	//引数:調べたいモデルのハンドル、レイキャスト変数
	//戻り値:なし
	void RayCast(int handle, RayCastData& data);

}

