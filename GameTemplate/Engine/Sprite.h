#pragma once
#include "Global.h"

class Sprite
{
	LPD3DXSPRITE         pSprite_;	//スプライト	板に画像のテクスチャを貼り付け
	LPDIRECT3DTEXTURE9 pTexture_;	//テクスチャ

public:
	Sprite();	
	~Sprite();

	void Load(const char* fileName);	//ファイル等からの読み込み処理
	void Draw(const D3DXMATRIX& matrix);//大きいデータなどを引数として受け取る際参照(&)を用いる	
										//テクスチャ等の描画処理
};

