#include "Text.h"
#include "Engine/Global.h"
#include "Engine/Direct3D.h"

//コンストラクタ
Text::Text(std::string fontName, int size) :pFont_(nullptr), color_(D3DCOLOR_ARGB(255, 255, 255, 255))
{
	D3DXCreateFont(
		Direct3D::pDevice,			//デバイスオブジェクト
		size,						//文字サイズ（高さ）
		0,							//文字サイズ（幅）
		FW_NORMAL,					//太さ
		1,							//MIPMAPのレベル
		FALSE,						//斜体
		DEFAULT_CHARSET,			//文字セット
		OUT_DEFAULT_PRECIS,			//出力精度
		DEFAULT_QUALITY,			//出力品質
		DEFAULT_PITCH | FF_SWISS,	//ピッチとファミリ
		fontName.c_str(),			//フォント名
		&pFont_);					//フォントオブジェクト

}

//デストラクタ
Text::~Text()
{
	SAFE_RELEASE(pFont_);
}

void Text::SetColor(DWORD r, DWORD g, DWORD b, DWORD alpha)
{
	color_ = D3DCOLOR_RGBA(r, g, b, alpha);
}


void Text::Draw(int x, int y, std::string text)
{
	RECT rect = { x, y, 0, 0 };
	pFont_->DrawText(NULL, text.c_str(), -1, &rect, DT_LEFT | DT_NOCLIP, color_);
}
