#define _CRT_SECURE_NO_WARNINGS

#include "Ball.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Collider.h"

#include <stdio.h>

#include "Wall.h"
#include "Ground.h"
#include "Text.h"

//コンストラクタ
Ball::Ball(IGameObject * parent)
	:IGameObject(parent, "Ball"), hModel_(-1), weight_(1.0f), vel_(0,0,0), acc_(0,grav,0), accFlag_(true), boundTime_(0.5f), isGround_(false)
{
}

//デストラクタ
Ball::~Ball()
{
}

//初期化
void Ball::Initialize()
{
	hModel_ = Model::Load("Data//Ball.fbx");

	boundTime_ = 1.0f;

	position_.y = 20.0f;

	SetCollider(position_, 1.0f);

	vel_.x = -0.2f;

<<<<<<< HEAD
	vel_.z = -0.2f;
=======
	//vel_.z = +0.2f;
>>>>>>> 08b8b86a03a768de3ca342a9e2a8c047c91932a4
	
	pCollider_->SetPos(position_);

	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 44);

	pText_->SetColor(0, 0, 0, 255);
}

//更新
void Ball::Update()
{
	Wall* pWall = (Wall*)(pParent_->FindChildObject("Wall"));

	for (int i = 0; i < WALL_MAX; i++)
	{
		hWalls_[i] = pWall->GetModelNumbers(i);
	}

	Ground* pGround = (Ground*)(pParent_->FindChildObject("Ground"));
	hGround_ = pGround->GetModelNumber();

	
	data.start = position_;

	data.direction = D3DXVECTOR3(0, -1, 0);

	Model::RayCast(hGround_, data);


	vel_ += acc_;

	position_ = position_ + vel_;

	pCollider_->SetPos(position_);

	


	//checkPos = position_ + ( vel_ + (acc_ * frameTime_) );

	snprintf(buf, 255, "%f", (double)position_.y);
}

//描画
void Ball::Draw()
{
	pText_->Draw((int)(600 / 2), 50, buf);

	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Ball::Release()
{
	delete pText_;
}

void Ball::OnCollision(IGameObject * pTarget, Collider * collTarget, Collider * collThis)
{
<<<<<<< HEAD
	D3DXVECTOR3 index;
	D3DXVec3Normalize(&index, &D3DXVECTOR3(0, 1, 0));

	if (pTarget->GetObjectName() ==  "Wall" || "Ground" )
=======
	if (data.hit)
>>>>>>> 08b8b86a03a768de3ca342a9e2a8c047c91932a4
	{
		D3DXVECTOR3 reflect;

<<<<<<< HEAD
		

		reflect = CalcParticlePlaneAfterPos(&position_, &vel_, 0.8f, boundTime_, &collTarget->GetNormal(), &position_, &vel_);

		if (D3DXVec3Length(&reflect) < 1)
		{
			/*acc_.y = 0;
			vel_.y = 0;*/
			isGround_ = true;
		}
		else
		{

			position_ = reflect;
			pCollider_->SetPos(position_);
		}

		position_ = reflect;
		pCollider_->SetPos(position_);

	}

	if (D3DXVec3Length(&vel_) > 0)
	{
		vel_ - D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	}

=======
		//反射計算
		reflect = CalcParticlePlaneAfterPos(&position_, &vel_, 0.9f, boundTime_, &collTarget->GetNormal(), &position_, &vel_);
		
		if (vel_.y > 1.0f)
		{
			//代入
			position_ = reflect;
			pCollider_->SetPos(position_);
		}
		else
		{
			acc_.y = 0;
			vel_.y = 0;
			position_ = reflect;
			pCollider_->SetPos(position_);
		}
	}
>>>>>>> 08b8b86a03a768de3ca342a9e2a8c047c91932a4
	
}

