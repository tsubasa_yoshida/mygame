#pragma once
#include "Engine/IGameObject.h"
#include "Physic.h"
#include "Engine/Fbx.h"

//重力加速度
const float grav = -9.8f / (1.0f * 60.0f);

#define IKD_EPSIRON 0.00001f // 誤差

class Text;

//ボールを管理するクラス
class Ball : public IGameObject
{
	//モデル
	int hModel_;

	//壁モデル番号受け取り用
	int hWalls_[4];

	//地面モデル番号受け取り用
	int hGround_;

	//球の重さ
	float weight_;

	float boundTime_;

	D3DXVECTOR3 acc_;

	D3DXVECTOR3 vel_;

	D3DXVECTOR3 prePos_;

	Text * pText_;

	Text * pText_2;

	bool accFlag_;

	bool isGround_;

	char buf[256];
<<<<<<< HEAD
=======

	RayCastData data;

>>>>>>> 08b8b86a03a768de3ca342a9e2a8c047c91932a4
public:
	//コンストラクタ
	Ball(IGameObject* parent);

	//デストラクタ
	~Ball();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(IGameObject* pTarget, Collider* collTarget, Collider* collThis)override;

	D3DXVECTOR3 GetVel() { return vel_; }

	D3DXVECTOR3 GetPrePos() { return prePos_; }

	D3DXVECTOR3 CalcParticlePlaneAfterPos(
		D3DXVECTOR3 *pColliPos,
		D3DXVECTOR3 *pVelo,
		FLOAT res,
		FLOAT time,
		D3DXVECTOR3 *pNormal,
		D3DXVECTOR3 *pOut_pos,
		D3DXVECTOR3 *pOut_velo
	)
	{

		// 反射後速度を算出
		D3DXVECTOR3 N = *pNormal;
		D3DXVec3Normalize(&N, pNormal);
		*pOut_velo = *pVelo - (1 + res)*D3DXVec3Dot(&N, pVelo)*N;

		return *pColliPos + *pOut_velo * time;

	}
};