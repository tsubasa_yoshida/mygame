#pragma once
#include "Engine/IGameObject.h"

#include <string.h>

using namespace std;

enum {
	WALL_R,
	WALL_L,
	WALL_F,
	WALL_B,
	WALL_MAX
};

//壁を管理するクラス
class Wall : public IGameObject
{
	int hModel_[WALL_MAX];	//モデル

	int wallNum_;

	D3DXVECTOR3 normal_;
	
public:
	//コンストラクタ
	Wall(IGameObject* parent);

	//デストラクタ
	~Wall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//法線セッター
	void SetNormal(D3DXVECTOR3 noraml);

	//番号セッター
	void SetNumber(int number);

	//モデル番号ゲッター
	int GetModelNumbers(int value);

	D3DXVECTOR3 GetNormal();
};