#include "TitleCamera.h"
#include "Engine/Direct3D.h"

//コンストラクタ
TitleCamera::TitleCamera(IGameObject * parent)
	:IGameObject(parent, "TitleCamera")
{
}

//デストラクタ
TitleCamera::~TitleCamera()
{
}

//初期化
void TitleCamera::Initialize()
{

	//回るの確認用に少し高めに配置中
	position_.x = 39.75f;
	position_.y = 7;
	position_.z = 78;

	rotate_.y = 180;


}

//更新
void TitleCamera::Update()
{
	D3DXVECTOR3 worldPosition, worldTarget;

	D3DXVec3TransformCoord(&worldPosition, &D3DXVECTOR3(0, 0, 0), &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &D3DXVECTOR3(0, -0.1, 0.5), &worldMatrix_);

	//ビュー行列を作るための関数(１、入れる箱　２、カメラの位置（視点）３、どこをみるか（焦点） 4上方向ベクトル（上を示す）)
	D3DXMatrixLookAtLH(&view_, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	//(1,ビュー行列として使ってほしい情報)
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view_);
}

//描画
void TitleCamera::Draw()
{
}

//開放
void TitleCamera::Release()
{
}