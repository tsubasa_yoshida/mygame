#pragma once

#include "Engine/global.h"


//分の値指定用　10からここで指定した数を引いた時間が設定される
//9と書いた場合　10 - 9 = 1 なので表示時間は一分から開始
#define MIN_COUNT 7	

#define THIEF1 1
#define THIEF2 2

//前方宣言
class Police;
class Thief;
class Text;
class Map;

enum {
	ACT_1,
	ACT_2,
	ACT_3
};

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	typedef struct 
	{
		D3DXVECTOR3 policePos;
		D3DXVECTOR3 thiefPos1P;
		D3DXVECTOR3 thiefPos2P;
	}SpawnPos;

	//スポーンポイント用配列
	SpawnPos spawnPos_[3];

	//各プレイヤーのポインタ
	Police* pPolice_;
	Thief*  pThief1_;
	Thief*  pThief2_;
	
	//テキスト
	Text* pText_;

	//ステージポインタ
	Map* pTempo_;

	//秒カウント用配列
	std::string sec_[11];	

	//分カウント用配列
	std::string min_[11];	

	//１のくらいのカウント用
	int cnt1_;

	//１０のくらいのカウント用
	int cnt10_;

	//分カウント配列の添字用
	int minCnt_;

	//フレームをカウントする変数
	int frame_;

	//救出可能かどうかの変数
	int rescueCnt_;

	//イメージ管理番号
	int hImage_[16];

	int sceneMigrationTime_;

	int actCnt_;

	int effectTime_;
	int deletePartitionTime_;

	float effectScale_;

	bool shief1Arrest_;

	bool shief2Arrest_;

	bool startGame_;

	int actCode_;

	int gameSetTime_;

	bool gameSet_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//救出可能かを見る関数
	void RescueCheck();

	void ActionCheck();

	void TimeCount();

	void StartGame();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void DrawSub()	override;

	void Efect(int thief);

	//救出中画像表示用
	void RescueEffect(int thief);

	//アクション中画像表示用
	void ActionEffect(int thief);

	//二人でアクションボタン押したとき画像描画用
	void PairActionEffect();



	//引数	: なし
	//戻り値	: なし
	//時間の初期化
	void InitTime();

	bool GetStartGame() { return startGame_; }
};
