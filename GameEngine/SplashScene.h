#pragma once

#include "Engine/global.h"
#include "Engine/Image.h"
//スプラッシュシーンを管理するクラス
class SplashScene : public IGameObject
{
	//フレーム観測用
	int countFrame_;
	int hPict_;
	int hPict2_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SplashScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};