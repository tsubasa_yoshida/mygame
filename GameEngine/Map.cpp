#include <assert.h>
#include <string>
#include "Engine/CsvReader.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"
#include "Map.h"
#include "Engine/Audio.h"

//コンストラクタ
Map::Map(IGameObject * parent)
	:IGameObject(parent, "Map"), ppTable_(nullptr), frame_(0), actCount_(0), deletePartition_(false)
{
}

//デストラクタ
Map::~Map()
{
}

//初期化
void Map::Initialize()
{
	std::vector<std::string> vecFileName;

	GetFileName(vecFileName,"Data/Model/Stage/Stage_FbxName.txt");

	//csvデータのロード	
	CsvReader csv;
	csv.Load("data/model/stage/Map80x80.csv");
	
	for (int i = 0; i < STAGE_MAX; i++)
	{
		hModel_[i] = 0;
	}

	hLModel_ = Model::Load("Data/Model/stage/Stage_Load.fbx");
	hL2Model_ = Model::Load("Data/Model/stage/Stage_Load2.fbx");
	hBModel_ = Model::Load("Data/Model/stage/Stage_BackGround.fbx");
	swichmodel_ = Model::Load("Data/Model/stage/Stage_switch.fbx");

	//ステージに登場するオブジェクト分ループしてロード
 	for (int i = 0; i < STAGE_MAX; i++)
	{
		hModel_[i] = Model::Load("data/model/stage/" + vecFileName.at(i));
	}

	//CSVファイルから縦幅横幅を受けとる
	csvWidth_ = csv.GetWidth();
	csvHeight_ = csv.GetHeight();
	
	

	//マップ読み込みのループ
	for (int x = 0; x < csvWidth_; x++)
	{
		for (int z = 0; z < csvHeight_; z++)
		{
		
			map_[x][z] = csv.GetValue(x, z);
			
		}
	}

}

//更新
void Map::Update()
{
}

//描画
void Map::Draw()
{

	Model::SetMatrix(hBModel_, worldMatrix_);
	Model::Draw(hBModel_);
	Model::SetMatrix(hLModel_, worldMatrix_);
	Model::Draw(hLModel_);
	Model::SetMatrix(hL2Model_, worldMatrix_);
	Model::Draw(hL2Model_);

	for (int x = 0; x < csvWidth_; x++)
	{
		for (int z = 0; z < csvHeight_; z++)
		{
			D3DXMATRIX mat;
			D3DXMatrixTranslation(&mat, (FLOAT)x, (FLOAT)0, (FLOAT)z);

			if (map_[x][z] > 99 && map_[x][z] < 107)
			{
				Model::SetMatrix(swichmodel_, mat);
				Model::Draw(swichmodel_);
			}


			//モデルの作り直しが必要
			if (
					(x == 1 && z == 1  && map_[x][z] == STAGE_AISLE && map_[x][z] < STAGE_MAX) ||
					(x == 0	&& z == 0  && map_[x][z] == STAGE_WALL	&& map_[x][z] < STAGE_MAX) ||
					(map_[x][z] != 0 && map_[x][z] != 1 && map_[x][z] < STAGE_MAX)
					
				)
			{
				D3DXMATRIX mat;
				D3DXMatrixTranslation(&mat, (FLOAT)x, (FLOAT)0, (FLOAT)z);
				
				Model::SetMatrix(hModel_[map_[x][z]], mat);
				Model::Draw(hModel_[map_[x][z]]);
				

			} 

		}
	}
}

//開放
void Map::Release()
{
	//for (int i = 0; i < csvWidth_; i++) 
	//{
	//	delete[] ppTable_[i];
	//}
	//
	//delete[] ppTable_;
}

void Map::GetFileName(std::vector<std::string> &vecFileName,std::string fileName)
{
	//ファイルを開く
	HANDLE hFile;
	hFile = CreateFile(fileName.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	//ファイルのサイズ（文字数）を調べる
	DWORD fileSize = GetFileSize(hFile, NULL);

	//すべての文字を入れられる配列を用意
	char* data;
	data = new char[fileSize];

	//ファイルの中身を配列に読み込む
	DWORD dwBytes = 0;
	ReadFile(hFile, data, fileSize, &dwBytes, NULL);

	//開いたファイルを閉じる
	CloseHandle(hFile);

	std::string fbxName;
	
	int i = 0;

	while (i < fileSize)
	{
		//特定文字がきたら改行コードがくるまでi++
		if (data[i] == '/')
		{
   		while (data[i] != '\r')
			{
				i++;
			}
		}
		//データがきたらいつも通りの処理
		else
		{
			int j  = 0;

			while (data[i + j] != '\r')
			 {
				j++;
			}

			std::string fbxFileName(data, i, j);

			vecFileName.push_back(fbxFileName);
			fbxFileName.clear();

			i += j;
		}
		
		// /nと/rの分をスキップ
		i += 2;
		
	}

	delete data;

}

void Map::DeletePartitionCenter()
{
	frame_++;

	if (frame_ == 60)
	{
		actCount_++;
		frame_ = 0;
	}

	if (actCount_ == 1)
	{
		for (int i = 1; i < 81; i++)
		{
			if (i != 61 && i != 20)
			{
				map_[i][41] = 0;
			}

		}

		map_[6][42] = 0;
		map_[10][42] = 0;
		map_[37][42] = 0;
		map_[41][42] = 0;
		map_[69][42] = 0;
		map_[73][42] = 0;

		frame_ = 0;
		actCount_ = 0;
		//マップ拡張
		Audio::Play("enlargement");
		deletePartition_ = true;
	}
}

void Map::DeletePartitionLeft()
{
	frame_++;

	if (frame_ == 60)
	{
		actCount_++;
		frame_ = 0;
	}

	if (actCount_ == 1)
	{
		for (int i = 1; i < 81; i++)
		{
			if (i != 41)
			{
				map_[20][i] = 0;
			}

		}

		map_[21][21] = 0;
		map_[21][26] = 0;
		map_[21][59] = 0;
		map_[21][63] = 0;

		frame_ = 0;
		actCount_ = 0;
		//マップ拡張
		Audio::Play("enlargement");
		deletePartition_ = true;

	}
}

void Map::DeletePartitionRight()
{
	frame_++;

	if (frame_ == 60)
	{
		actCount_++;
		frame_ = 0;
	}

	if (actCount_ == 1)
	{
		for (int i = 1; i < 81; i++)
		{
			if (i != 41)
			{
				map_[61][i] = 0;
			}

		}

		map_[60][21] = 0;
		map_[60][26] = 0;
		map_[60][59] = 0;
		map_[60][63] = 0;

		frame_ = 0;
		actCount_ = 0;
		//マップ拡張
		Audio::Play("enlargement");
		deletePartition_ = true;

	}
}
