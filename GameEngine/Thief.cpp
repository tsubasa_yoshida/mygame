#include "Thief.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"
#include "Map.h"
#include "Engine/Image.h"
#include "Engine/Input.h"
#include <string>
#include "Engine/Audio.h"
//コンストラクタ
Thief::Thief(IGameObject * parent)
	:IGameObject(parent, "Thief"), hModel_(-1), radius_(0.5), isArrest_(false), killEffectTime_(0),
	rescueCnt_(0), frame_(0), isRescue_(false), isAct_(false), permissionMove_(false), isTitle_(false), stamina_(100)

{
}

//デストラクタ
Thief::~Thief()
{
}

//初期化
void Thief::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/model/player/Player_Thief_M.fbx");
	assert(hModel_ >= 0);

	std::string str = pParent_->GetObjectName();

	if (str == "PlayScene")
	{
		//当たり判定の追加
		SetCollider(D3DXVECTOR3(1, 1, 1), 0.5);
	}

	//壁との当たり判定
	pTempo_ = (Map*)pParent_->FindChildObject("Map");

	circlePos_.PosX = position_.x;
	circlePos_.PosY = position_.z;
	circlePos_.Angle = 1;
	circlePos_.Length = 3.5;
	circlePos_.CenterX = 39.5;
	circlePos_.CenterY = 61.5;

	Model::SetAnimFrame(hModel_, 0, 80, 0.5f);
}

//更新
void Thief::Update()
{

	if (isTitle_)
	{
		CreateCircle();
	}
	else
	{
		//捕まっているかどうか
		switch (isArrest_)
		{
			//捕まった場合は檻の場所へ移動
		case TRUE:
			position_.x = CAGE_POS_X;
			position_.y = CAGE_POS_Y;
			position_.z = CAGE_POS_Z;

			look();

			if (killEffectTime_ > 0)
			{
				killEffectTime_--;
			}
			break;
		case FALSE:
			if (permissionMove_)
			{
				Move();
			}
			WallCollision();
			break;
		}
		Transform();

		ThiefView();
	}



	
	
	stamina_ += (1 / 60);
	
}

void Thief::CreateCircle()
{
	//半径３．５
	//x40
	//z62
	//

	// 中心座標に角度と長さを使用した円の位置を加算する
	// 度数法の角度を弧度法に変換
	float radius = circlePos_.Angle * 3.14 / 180.0f;

	// 三角関数を使用し、円の位置を割り出す。
	float addX = cos(radius) * circlePos_.Length;
	float addY = sin(radius) * circlePos_.Length;

	// 結果ででた位置を中心位置に加算し、それを描画位置とする
	position_.x = circlePos_.CenterX + addX;
	position_.z = circlePos_.CenterY + addY;

	circlePos_.Angle += 1;

	rotate_.y -= 1;

}

void Thief::ThiefView()
{
	D3DXVECTOR3 worldPosition, worldTarget;

	D3DXVec3TransformCoord(&worldPosition, &D3DXVECTOR3(0, 1.0f, 0), &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &D3DXVECTOR3(0, 1.0f, 1), &worldMatrix_);

	//ビュー行列を作るための関数(１、入れる箱　２、カメラの位置（視点）３、どこをみるか（焦点） 4上方向ベクトル（上を示す）)
	D3DXMatrixLookAtLH(&view_, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	//(1,ビュー行列として使ってほしい情報)
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view_);
}

//描画
void Thief::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Thief::Release()
{
}

//衝突検知用関数
void Thief::OnCollision(IGameObject * pTarget)
{

	//警察とぶつかった時の処理
	if (pTarget->GetObjectName() == "Police")
	{
	
		//確保の音
		Audio::Play("Touch");
		isArrest_ = true;
		killEffectTime_ = 100;
	}

}

void Thief::WallCollision()
{//チェックするマス
	int checkX;
	int checkZ;

	//左の判定
	checkX = (int)(position_.x - radius_);
	checkZ = (int)position_.z;
	if (pTempo_->IsImPassable(checkX, checkZ))
	{
		position_.x = checkX + 1 + radius_;

	}

	//右の判定
	checkX = (int)(position_.x + radius_);
	checkZ = (int)position_.z;
	if (pTempo_->IsImPassable(checkX, checkZ))
	{
		position_.x = checkX - radius_;
	}

	//奥の判定
	checkX = (int)position_.x;
	checkZ = (int)(position_.z + radius_);
	if (pTempo_->IsImPassable(checkX, checkZ))
	{
		position_.z = checkZ - radius_;
	}

	//後ろの判定
	checkX = (int)position_.x;
	checkZ = (int)(position_.z - radius_);
	if (pTempo_->IsImPassable(checkX, checkZ))
	{
		position_.z = checkZ + 1 + radius_;
	}


}

void Thief::Move()
{
	D3DXMATRIX move;
	D3DXMatrixRotationY(&move, D3DXToRadian(rotate_.y));


	D3DXVECTOR3 UV = D3DXVECTOR3(0, 0, 0.04f);
	D3DXVec3TransformCoord(&UV, &UV, &move);

	D3DXVECTOR3 LR = D3DXVECTOR3(0.04f, 0, 0);
	D3DXVec3TransformCoord(&LR, &LR, &move);

	////だっしゅ
	//D3DXVECTOR3 UV = D3DXVECTOR3(0, 0, 0.07f);
	//D3DXVec3TransformCoord(&UV, &UV, &move);

	//D3DXVECTOR3  LR = D3DXVECTOR3(0.07f, 0, 0);
	//D3DXVec3TransformCoord(& LR, & LR, &move);

	//スティック操作
	D3DXVECTOR3 stick = Input::GetPadStickL(pID_);

	
		

	//////////////////////////////////
	/*  ↓  ここから通常移動   ↓   */
	//////////////////////////////////

	//右斜め前移動
	if (stick.z > 0 && stick.x > 0)
	{
		position_ += UV / 1.5;
		position_ += LR / 1.5;
	}
	//左斜め後ろ移動
	else if (stick.z < 0 && stick.x < 0)
	{
		position_ -= UV / 1.5;
		position_ -= LR / 1.5;
	}
	//左斜め前移動
	else if (stick.z > 0 && stick.x < 0)
	{
		position_ += UV / 1.5;
		position_ -= LR / 1.5;
	}
	//右斜め後ろ移動
	else if (stick.z < 0 && stick.x > 0)
	{
		position_ -= UV / 1.5;
		position_ += LR / 1.5;
	}
	//前移動
	else if (stick.z > 0)
	{
		position_ += UV;
	}
	//後ろ移動
	else if (stick.z < 0)
	{
		position_ -= UV;
	}
	//左移動
	else if (stick.x > 0)
	{
		position_ += LR;
	}
	//右移動
	else if (stick.x < 0)
	{
		position_ -= LR;
	}
	


	

	//if (stamina_ > 10)
	//{
	//	//Rボタンでだっしゅ
	//	if (stick.z > 0 && Input::IsPadButton(XINPUT_GAMEPAD_RIGHT_SHOULDER, pID_))
	//	{

	//		stamina_ -= (10 / 60);

	//		position_ += UV;
	//	}

	//}



	{
		//右スティックで回転(視点移動)
		D3DXVECTOR3 stick = Input::GetPadStickR(pID_);

		rotate_.y += (stick.x / 0.65f);
	}

	//→が押されていたら
	if (Input::IsKey(DIK_RIGHT))
	{
		//右へ移動
		position_.x += 0.1f;

	}

	//←が押されていたら
	if (Input::IsKey(DIK_LEFT))
	{
		//左へ移動
		position_.x -= 0.1f;

	}

	//↑が押されていたら
	if (Input::IsKey(DIK_UP))
	{
		//上へ移動
		position_.z += 0.1f;
	}

	//↓が押されていたら
	if (Input::IsKey(DIK_DOWN))
	{
		//下へ移動
		position_.z -= 0.1f;

	}


	//Dキーが押されていたら
	if (Input::IsKey(DIK_RETURN))
	{
		//時計回りに回転
		rotate_.y += 1.0f;
	}

	//Aキーが押されていたら
	if (Input::IsKey(DIK_RSHIFT))
	{
		//反時計回りに回転
		rotate_.y -= 1.0f;
	}

}

void Thief::look()
{
	{
		//右スティックで回転(視点移動)
		D3DXVECTOR3 stick = Input::GetPadStickR(pID_);

		rotate_.y += (stick.x / 0.65f);
	}

	//Enterキーが押されていたら
	if (Input::IsKey(DIK_RETURN))
	{
		//時計回りに回転
		rotate_.y += 1.0f;
	}

	//右Shiftキーが押されていたら
	if (Input::IsKey(DIK_RSHIFT))
	{
		//反時計回りに回転
		rotate_.y -= 1.0f;
	}
}

//救出処理
void Thief::Rescue(Thief* &thief)
{
	if (Input::IsKey(DIK_SPACE) || Input::IsPadButton(XINPUT_GAMEPAD_A, pID_))
	{
		//自分が救出可能なマスに居るかの判定
		if (pTempo_->IsRescuePoint(position_.x, position_.z))
		{
			//救出状態に移行
			isRescue_ = true;
			frame_++;
			if (frame_ == 60)
			{
				rescueCnt_++;
				frame_ = 0;
			}

			//3秒間救出ボタンを押されたら檻から出す処理
			if (rescueCnt_ == 2)
			{
				//逃げる
				Audio::Play("Escape");
				//確保状態から逃走状態へ更新
				thief->isArrest_ = false;
				isRescue_ = false;
				thief->SetPosition(position_);
				rescueCnt_ = 0;
			}
		}
		else
		{
			isRescue_ = false;
		}
	}
	else
	{
		frame_ = 0;
		rescueCnt_ = 0;
		isRescue_ = false;
	}
}


void Thief::IsAction(Thief* &thief)
{
	//SPACEかPADのAボタンが押されたら
	if (Input::IsKey(DIK_SPACE) || Input::IsPadButton(XINPUT_GAMEPAD_A, pID_))
	{
		if (((int)position_.x != (int)thief->GetPosition().x) || ((int)position_.z != (int)thief->GetPosition().z))
		{
			//自分がアクション可能なマスに居るかの判定
			if (pTempo_->IsActionPoint(position_.x, position_.z))
			{
				isAct_ = true;
				actCode_ = pTempo_->GetActCode(position_.x, position_.z);
			}
			//アクション可能なマスにいなければfalse
			else
			{
				isAct_ = false;
			}
		}
		//もう一人の泥棒がボタンを押していなければfalse
		else
		{
			isAct_ = false;
		}

	}
	//ボタンが押されていなければfalse
	else
	{
		isAct_ = false;
	}
}

