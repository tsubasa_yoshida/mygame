#pragma once
#include"Engine/IGameObject.h"
#include "Engine/Global.h"
#include <vector>

//
#define MOVING_DISABLED 1

enum
{
	STAGE_RESCUE = 99,
	STAGE_ACT_CENTER_1,
	STAGE_ACT_CENTER_2,
	STAGE_ACT_CENTER_3,
	STAGE_ACT_LEFT_1,
	STAGE_ACT_LEFT_2,
	STAGE_ACT_RIGHT_1,
	STAGE_ACT_RIGHT_2,
	STAGE_SWICT
};

//マップを管理するクラス
class Map : public IGameObject
{
	enum
	{
		STAGE_AISLE,
		STAGE_WALL,
		STAGE_POAL,
		STAGE_GATE,
		STAGE_FOUTAIN,
		STAGE_JUNGLEGYM,
		STAGE_TIRE,
		STAGE_SPRING,
		STAGE_BURANKO,
		STAGE_TETSUBO,
		STAGE_SLIDER,
		STAGE_CENTER,
		STAGE_LEFTRIGHT,
		STAGE_WOOD,
		STAGE_Garbage_Can,
		STAGE_ROCK,
		STAGE_BENTI,
		STAGE_ROAD,
		STAGE_RABYRINTH,
		STAGE_VENDINGMACHINE,
		STAGE_STATUE,
		STAGE_TOILET,
		STAGE_RAGPOLE,
		STAGE_GOAL_L,
		STAGE_GOAL_R,
		STAGE_TABLE_AND_CHAIR,
		STAGE_BENTI_R,
		STAGE_MIZUNOMIBA,
		STAGE_HEDGE_V,
		STAGE_HEDGE_H,
		STAGE_OBJE,
		STAGE_MAX	
	};



	int frame_;

	int actCount_;

	//各モデル番号
	int hModel_[STAGE_MAX];
	
	//地面専用モデ ル番号
	int hGModel_;
	int hBModel_;
	int swichmodel_;
	int hLModel_;
	int hL2Model_;

	//マップを動的確保する際のダブルポインタ
	int** ppTable_;
	int map_[83][83];

	//マップ横幅と縦幅
	int csvWidth_;
	int csvHeight_;

	//壁が消えたか否か
	bool deletePartition_;

public:
	//コンストラクタ
	Map(IGameObject* parent);

	//デストラクタ
	~Map();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
	
	//引数	: Police,Thiefから渡された見てほしい座標
	//戻り値	: 通れないマスだったらtrue
	//通れないマスかを判定する関数
	bool IsImPassable(int x, int z)
	{
	
		if (map_[x][z] != STAGE_AISLE && map_[x][z] != STAGE_RESCUE && map_[x][z] != STAGE_ACT_CENTER_1 &&
			map_[x][z] != STAGE_ACT_CENTER_2 && map_[x][z] != STAGE_ACT_CENTER_3 && map_[x][z] != STAGE_ACT_LEFT_1 &&
			map_[x][z] != STAGE_ACT_LEFT_2 && map_[x][z] != STAGE_ACT_RIGHT_1 && map_[x][z] != STAGE_ACT_RIGHT_2 &&
			map_[x][z] != STAGE_ROAD)
		{
			return true;
		}
		 
		return false;
		
	}

	//引数	: Thiefが居る座標
	//戻り値	: 檻の周り1マス以内だったらtrue
	//救出可能マスかの判定をする関数
	bool IsRescuePoint(int x, int z)
	{
		return map_[x][z] == STAGE_RESCUE;
	}

	//引数	: Thiefが居る座標
	//戻り値	: アクションマス上であればtrue
	//アクションマスかの判定をする関数
	bool IsActionPoint(int x, int z)
	{
		if (map_[x][z] == STAGE_ACT_CENTER_1 || map_[x][z] == STAGE_ACT_LEFT_1 || map_[x][z] == STAGE_ACT_RIGHT_1 ||
			map_[x][z] == STAGE_ACT_CENTER_2 || map_[x][z] == STAGE_ACT_LEFT_2 || map_[x][z] == STAGE_ACT_RIGHT_2 ||
			map_[x][z] == STAGE_ACT_CENTER_3)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	void GetFileName(std::vector<std::string> &vecFileName,std::string fileName);

	void DeletePartitionCenter();

	void DeletePartitionLeft();

	void DeletePartitionRight();

	int GetActCode(int x, int z) { return map_[x][z]; }

	bool GetDeletePartition() { return deletePartition_; }
	void SetDeletePartition(bool TorF) { deletePartition_ = TorF; }
};

