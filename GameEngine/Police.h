#pragma once
#include "Engine/IGameObject.h"
#include "Map.h"

class Map;


//警察を管理するクラス
class Police : public IGameObject
{
	int hModel_;    //モデル番号

	int pID_;		//コントローラーの識別番号

	float radius_;  //キャラの半径

	Map* pTempo_;	//

	D3DXMATRIX view_; //メンバビュー

	bool permissionMove_;

	DrawObject circlePos_;

	bool isTitle_;

	float stamina_;

public:
	static D3DXMATRIX policeView_;

	//コンストラクタ
	Police(IGameObject* parent);

	//デストラクタ
	~Police();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//円をえがいて動く関数
	void CreateCircle();

	void PoliceView();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//プレイヤーの保持しているID受け取り用
	void SetPlayerID(int id) { pID_ = id; }
	
	void WallCollision();

	void Move();

	void SetTitle(bool flag) { isTitle_ = flag; };

	//プレイヤのビュー受け取り用
	D3DXMATRIX GetView() { return view_; }

	void SetPermissionMove(bool flag) { permissionMove_ = flag; }
};