#pragma once

#include "Engine/IGameObject.h"

class Map;

#define CAGE_POS_X 50
#define CAGE_POS_Y 0.3f
#define CAGE_POS_Z 47

//泥棒を管理するクラス
class  Thief : public IGameObject
{
	int hModel_;    //モデル番号

	int pID_;		//コントローラーの割り当ての番号

	float radius_;  //キャラの半径

	bool isArrest_; //捕まっているかの判定用

	int killEffectTime_; //エフェクト表示時間

	int rescueCnt_;	//救出時に一定時間カウント用

	int frame_;		//60フレーム(一秒)単位での処理用

	int actCode_;	//アクションコード格納用

	bool isRescue_;	//救出中かどうかの判定用

	bool isAct_;	//アクションボタンが押されているかどうか

	Map* pTempo_; // マップのポインタ

	D3DXMATRIX view_; //メンバビュー

	bool permissionMove_;

	DrawObject circlePos_;

	float stamina_;

	bool isTitle_;

public:
	//コンストラクタ
	Thief(IGameObject* parent);

	//デストラクタ
	~Thief();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//円をえがいて動く関数
	void CreateCircle();

	void ThiefView();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//プレイヤーの保持しているID受け取り用
	void SetPlayerID(int id) { pID_ = id; }

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;

	void WallCollision();

	void Move();

	//捕まった泥棒のカメラ
	void look();

	//救出処理関数
	void Rescue(Thief* &thief);

	//アクション実行処理関数
	void IsAction(Thief* &thief);

	//プレイヤのビュー受け取り用
	D3DXMATRIX GetView() { return view_; }

	//泥棒が捕まっているかどうかの状態受け取り用
	bool IsArrest() { return isArrest_; }

	int GetKillEffectTime() { return killEffectTime_; }

	//救出をしているかどうかの受け取り用
	bool GetIsRescue() { return isRescue_; }

	//アクションをしているかどうかの受け取り用
	bool GetIsAct() { return isAct_; }

	//どのアクションをしているかの受け取り用
	int GetActCode() { return actCode_; }

	//アクション処理終了時のフラグリセット
	void SetActClear() { isAct_ = false; }

	void SetPermissionMove(bool flag) { permissionMove_ = flag; }

	void SetTitle(bool flag) { isTitle_ = flag; };
};