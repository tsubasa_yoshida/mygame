#pragma once
#include "Engine/global.h"

#define COUNT_NUM 6
#define FRAME_MIN 0
#define FRAME_MAX 60

//前方宣言
class Text;

// 警察勝利時のリザルトシーンを管理するクラス
class PoliceWinResult : public IGameObject
{
	int hImage_[6];

		Text* pText_;	//テキスト
		Text* pText2_;
		Text* pText3_;

		std::string sec_[COUNT_NUM];		//秒カウント用

		int cnt_;		//秒カウント要素数用

		int frame_;		//フレームカウント用変数

		int temp;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PoliceWinResult(IGameObject* parent);

	void Initialize() override;

	void Update() override;

	void Draw() override;

	void Release() override;

	//描画(画面分割用)
	void DrawSub()	override;
};