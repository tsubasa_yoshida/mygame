#include "PlayScene.h"
#include <string>
#include <time.h>
#include "Engine/SceneManager.h"
#include "Engine/Direct3D.h"
#include "Engine/Audio.h"
#include "Engine/Camera.h"
#include "Engine/Image.h"
#include "Engine/Text.h"
#include "Map.h"
#include "Police.h"
#include "Thief.h"



//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), pText_(nullptr), cnt1_(0), cnt10_(0), minCnt_(MIN_COUNT), frame_(0), sceneMigrationTime_(0), effectTime_(0), effectScale_(1)
	, shief1Arrest_(0), shief2Arrest_(0), startGame_(false), actCode_(-1), actCnt_(0), gameSetTime_(0), gameSet_(false), deletePartitionTime_(0)
{
}

//初期化
void PlayScene::Initialize()
{

	////////////////////////////
	spawnPos_[0].policePos.x = 41;
	spawnPos_[0].policePos.y = 0;
	spawnPos_[0].policePos.z = 80;

	spawnPos_[0].thiefPos1P.x = 38;
	spawnPos_[0].thiefPos1P.y = 0;
	spawnPos_[0].thiefPos1P.z = 47;

	spawnPos_[0].thiefPos2P.x = 42;
	spawnPos_[0].thiefPos2P.y = 0;
	spawnPos_[0].thiefPos2P.z = 47;

	/////////////////////////////
	spawnPos_[1].policePos.x = 30;
	spawnPos_[1].policePos.y = 0;
	spawnPos_[1].policePos.z = 7;

	spawnPos_[1].thiefPos1P.x = 39;
	spawnPos_[1].thiefPos1P.y = 0;
	spawnPos_[1].thiefPos1P.z = 39;

	spawnPos_[1].thiefPos2P.x = 1;
	spawnPos_[1].thiefPos2P.y = 0;
	spawnPos_[1].thiefPos2P.z = 39;

	/////////////////////////////
	spawnPos_[2].policePos.x = 6;
	spawnPos_[2].policePos.y = 0;
	spawnPos_[2].policePos.z = 6;

	spawnPos_[2].thiefPos1P.x = 6;
	spawnPos_[2].thiefPos1P.y = 0;
	spawnPos_[2].thiefPos1P.z = 37;

	spawnPos_[2].thiefPos2P.x = 38;
	spawnPos_[2].thiefPos2P.y = 0;
	spawnPos_[2].thiefPos2P.z = 2;

	//マップの登場
	pTempo_ = CreateGameObject<Map>(this);

	srand((unsigned int)time(0));

	int spawn = rand() % 3;

	//警察を登場させる
	pPolice_ = CreateGameObject<Police>(this);
	pPolice_->SetPlayerID(0);
	pPolice_->SetPosition(spawnPos_[0].policePos);

	//泥棒１を登場させる
	pThief1_ = CreateGameObject<Thief>(this);
	pThief1_->SetPlayerID(1);
	pThief1_->SetPosition(spawnPos_[0].thiefPos1P);

	//泥棒2を登場させる
	pThief2_ = CreateGameObject<Thief>(this);
	pThief2_->SetPlayerID(2);
	pThief2_->SetPosition(spawnPos_[0].thiefPos2P);

	//ファイルネームを登録
	std::string fileName[16] =
	{
		"Data/pict/PoliceFrame.png",
		"Data/pict/ThiefFrame.png",
		"Data/pict/ThiefFrame1.png",
		"Data/pict/Atumori.png",
		"Data/pict/MiniPolice.png",
		"Data/pict/kakuho.png",
		"Data/pict/taiho.png",
		"Data/pict/rescue.png",
		"Data/pict/1.png",
		"Data/pict/2.png",
		"Data/pict/3.png",
		"Data/pict/GAMESTART.png",
		"Data/pict/GameSet.png",
		"Data/pict/Thief_Action.png",
		"Data/pict/Pair_Thief_Action.png",
		"Data/pict/kaihou.png"
	};

	//登録した画像を読み込み
	for (int i = 0; i < 16; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}

	
	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 88);

	pText_->SetColor(0, 0, 0, 255);    //文字色を赤にする

	//時間をセット
	InitTime();
	Audio::Play("Play_BGM");
}



//更新
void PlayScene::Update()
{


	//泥棒が二人とも捕まっていた場合警察勝利リザルトへ
	if ((pThief1_->IsArrest() == true && pThief2_->IsArrest() == true) && gameSetTime_ > 120)
	{
		sceneMigrationTime_++;

		if (sceneMigrationTime_ > 60)
		{
			gameSetTime_ = 0;
			SceneManager::ChangeScene(SCENE_ID_POLICEWIN);
		}
	}

	if (min_[minCnt_] == "0" && sec_[cnt10_] == "0" && sec_[cnt1_] == "0")
	{
		gameSet_ = true;
		pPolice_->SetPermissionMove(false);
		pThief1_->SetPermissionMove(false);
		pThief2_->SetPermissionMove(false);
	}

	//制限時間が0になったら各リザルトに移行
	if (gameSet_ && gameSetTime_ > 120)
	{
		gameSetTime_ = 0;
		gameSet_ = false;

		//泥棒が二人とも捕まっていた場合警察勝利リザルトへ
		if (pThief1_->IsArrest() == true && pThief2_->IsArrest() == true)
		{
			startGame_ = false;
			SceneManager::ChangeScene(SCENE_ID_POLICEWIN);
		}

		//泥棒のどちらかが逃げ切れた場合は泥棒勝利リザルトへ
		else
		{
			startGame_ = false;

			SceneManager::ChangeScene(SCENE_ID_THIEFWIN);
		}
	}

	if (startGame_ && !gameSet_)
	{
		TimeCount();
	}
	RescueCheck();


	//泥棒がボタンを同時にをした時の処理
	pThief1_->IsAction(pThief2_);
	pThief2_->IsAction(pThief1_);


	ActionCheck();

	if (actCode_ == ACT_1)
	{
		//壁消去処理
		pTempo_->DeletePartitionCenter();
	}
	else if (actCode_ == ACT_2)
	{
		//壁消去処理
		pTempo_->DeletePartitionLeft();

	}
	else if (actCode_ == ACT_3)
	{
		//壁消去処理
		pTempo_->DeletePartitionRight();
	}

	if (pTempo_->GetDeletePartition())
	{
		deletePartitionTime_ = 180;
		pTempo_->SetDeletePartition(false);
	}
}

void PlayScene::RescueCheck()
{
	if (pThief1_->IsArrest() == true)
	{
		pThief2_->Rescue(pThief1_);
	}
	else if (pThief2_->IsArrest() == true)
	{
		pThief1_->Rescue(pThief2_);
	}
}

void PlayScene::ActionCheck()
{
	if (pThief1_->GetActCode() == pThief2_->GetActCode())
	{
		if (pThief1_->GetIsAct() && pThief2_->GetIsAct())
		{
			if (pThief1_->GetActCode() == STAGE_ACT_CENTER_1 || pThief1_->GetActCode() == STAGE_ACT_CENTER_2 || pThief1_->GetActCode() == STAGE_ACT_CENTER_3 || 
				pThief2_->GetActCode() == STAGE_ACT_CENTER_1 || pThief2_->GetActCode() == STAGE_ACT_CENTER_2 || pThief2_->GetActCode() == STAGE_ACT_CENTER_3)
			{
				actCode_ = ACT_1;
			}
			else if (pThief1_->GetActCode() == STAGE_ACT_LEFT_1 || pThief1_->GetActCode() == STAGE_ACT_LEFT_2 ||
					 pThief2_->GetActCode() == STAGE_ACT_LEFT_1 || pThief2_->GetActCode() == STAGE_ACT_LEFT_2)
			{
				actCode_ = ACT_2;
			}
			else if (pThief1_->GetActCode() == STAGE_ACT_RIGHT_1 || pThief1_->GetActCode() == STAGE_ACT_RIGHT_2 ||
					 pThief2_->GetActCode() == STAGE_ACT_RIGHT_1 || pThief2_->GetActCode() == STAGE_ACT_RIGHT_2)
			{
				actCode_ = ACT_3;
			}
		}
		else
		{
			actCode_ = -1;
		}
	}
}



void PlayScene::TimeCount()
{
	//フレームのカウントが60でちょうど1秒
	frame_++;

	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{

			//最初3:00から2:59にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 5;
				cnt1_ = 0;
				minCnt_++;
			}



			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 5;
					cnt1_ = 1;
					minCnt_++;
				}

				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}

				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}

			}

		}
	}
}


//描画
void PlayScene::Draw()
{

}

//開放
void PlayScene::Release()
{
	delete pText_;
	Audio::Stop("Play_BGM");
}

void PlayScene::DrawSub()
{
	StartGame();

	//画像の描画
	for (int i = 0; i < 3; i++)
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, (FLOAT)i * 640, 0, 0);

		D3DXMATRIX a;
		D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);
		Image::SetMatrix(hImage_[i], localMatrix_ * m * a);
		Image::Draw(hImage_[i]);
	}

	{
		D3DXMATRIX m, ms;
		D3DXMatrixScaling(&ms, (FLOAT)0.1, (FLOAT)0.1, (FLOAT)0.1);
		D3DXMatrixTranslation(&m, 540, (FLOAT)(g.screenHeight / 3) - 360, 0);

		D3DXMATRIX a;
		D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

		Image::SetMatrix(hImage_[4], localMatrix_ * ms * m * a);
		Image::Draw(hImage_[4]);
		if (shief2Arrest_)
		{
			Image::SetMatrix(hImage_[5], localMatrix_ * ms * m * a);
			Image::Draw(hImage_[5]);
		}

		D3DXMatrixTranslation(&m, 480, (FLOAT)(g.screenHeight / 3) - 360, 0);
		Image::SetMatrix(hImage_[4], localMatrix_ * ms * m * a);
		Image::Draw(hImage_[4]);

		if (shief1Arrest_)
		{
			Image::SetMatrix(hImage_[5], localMatrix_ * ms * m * a);
			Image::Draw(hImage_[5]);
		}
	}

	//壁が解放された時のエフェクト
	if (deletePartitionTime_ > 0)
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, (FLOAT)0, g.screenHeight / 3, 0);

		D3DXMATRIX a;
		D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);
		Image::SetMatrix(hImage_[15], localMatrix_ * m * a);
		Image::Draw(hImage_[15]);
		deletePartitionTime_--;

	}

	//犯人が捕まった時のエフェクト（熱盛）
	if (pThief1_->IsArrest() == true && pThief1_->GetKillEffectTime() > 0)
	{
		Efect(THIEF1);
	}

	if (pThief2_->IsArrest() == true && pThief2_->GetKillEffectTime() > 0)
	{
		Efect(THIEF2);
	}

	//救出中画像表示
	if (pThief1_->GetIsRescue())
	{
		RescueEffect(THIEF1);
	}

	if (pThief2_->GetIsRescue())
	{
		RescueEffect(THIEF2);
	}

	if (actCode_ == -1)
	{
		//個人でアクションボタンを押しているときの画像表示処理部分
		if (pThief1_->GetIsAct())
		{
			ActionEffect(THIEF1);
		}

		if (pThief2_->GetIsAct())
		{
			ActionEffect(THIEF2);
		}
	}
	else
	{
		PairActionEffect();
	}

	

	//警察が捕まえた時のエフェクト
	if (pThief1_->IsArrest() == true || pThief2_->IsArrest() == true)
	{
		D3DXMATRIX m, ms;
		D3DXMatrixScaling(&ms, effectScale_, effectScale_, effectScale_);

		if (effectTime_ > 0 && effectTime_ < 60)
		{
			if (pThief1_->IsArrest() == true)
			{
				D3DXMatrixTranslation(&m, (FLOAT)(effectTime_* 0.9) * 9, (FLOAT)(g.screenHeight / 3) - (effectTime_ * 6), 0);
			}
			if (pThief2_->IsArrest() == true)
			{
				D3DXMatrixTranslation(&m, (FLOAT)(effectTime_) * 9, (FLOAT)(g.screenHeight / 3) - (effectTime_ * 6), 0);
			}
			effectScale_ -= 0.015f;
		}
		else if (effectTime_ >= 60)
		{
			if (pThief1_->IsArrest() == true)
			{
				shief1Arrest_ = true;
			}
			if (pThief2_->IsArrest() == true)
			{
				shief2Arrest_ = true;
			}
		}
		else
		{
			D3DXMatrixTranslation(&m, 0, (FLOAT)g.screenHeight / 3, 0);
		}

		D3DXMATRIX a;
		D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

		Image::SetMatrix(hImage_[5], localMatrix_ * ms * m * a);
		Image::Draw(hImage_[5]);

		effectTime_++;
	}

	if (pThief1_->IsArrest() == false)
	{
		shief1Arrest_ = false;
	}
	if (pThief2_->IsArrest() == false)
	{
		shief2Arrest_ = false;
	}

	

	//第一引数は横幅、第二引数は縦幅位置情報
	pText_->Draw(1920 * 3 / 7, 50, min_[minCnt_] + ":" + sec_[cnt10_] + sec_[cnt1_]); //自動で2:59にする
	pText_->Draw((int)(1920 * 3 / 2.1), 50, min_[minCnt_] + ":" + sec_[cnt10_] + sec_[cnt1_]); //自動で2:59にする
	pText_->Draw((int)(1920 * 3 / 1.22), 50, min_[minCnt_] + ":" + sec_[cnt10_] + sec_[cnt1_]); //自動で2:59にする

	//左画面
	{

		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = 0;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//ビュー行列
		Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pPolice_->GetView());

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}

	}

	//中心画面
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = g.screenWidth / 3;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//ビュー行列
		Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pThief1_->GetView());

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}

	}

	//右画面
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = g.screenWidth * 2 / 3;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//ビュー行列
		Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pThief2_->GetView());

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}
	}

	//戻す
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = 0;
		vp.Y = 0;
		vp.Width = g.screenWidth;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);
	}

	//ゲームセット表示
	{
		if ((pThief1_->IsArrest() == true && pThief2_->IsArrest() == true) || gameSet_)
		{
			gameSetTime_++;
			for (int i = 0; i < 3; i++)
			{
				D3DXMATRIX m;
				D3DXMatrixTranslation(&m, (FLOAT)i * 640, g.screenHeight / 3, 0);

				D3DXMATRIX a;
				D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);
				Image::SetMatrix(hImage_[12], localMatrix_ * m * a);
				Image::Draw(hImage_[12]);
			}
		}
	}
}

void PlayScene::Efect(int thief)
{


	D3DXMATRIX m;

	D3DXMatrixTranslation(&m, (FLOAT)thief * (g.screenWidth / 3), (FLOAT)g.screenHeight / 3, 0);
	D3DXMATRIX a;
	D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

	Image::SetMatrix(hImage_[6], localMatrix_ * a * m);
	Image::Draw(hImage_[6]);

	effectTime_ = 0;
	effectScale_ = 1;



}

void PlayScene::RescueEffect(int thief)
{
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (FLOAT)thief * (g.screenWidth / 3), (FLOAT)g.screenHeight / 3, 0);

	D3DXMATRIX a;
	D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

	Image::SetMatrix(hImage_[7], localMatrix_ * a * m);
	Image::Draw(hImage_[7]);
}

void PlayScene::ActionEffect(int thief)
{
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (FLOAT)thief * (g.screenWidth / 3), (FLOAT)g.screenHeight / 3, 0);
	D3DXMATRIX a;
	D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

	Image::SetMatrix(hImage_[13], localMatrix_ * a * m);
	Image::Draw(hImage_[13]);
}

void PlayScene::PairActionEffect()
{
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, (FLOAT)THIEF1 * (g.screenWidth / 3), (FLOAT)g.screenHeight / 3, 0);

	D3DXMATRIX a;
	D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

	Image::SetMatrix(hImage_[14], localMatrix_ * a * m);
	Image::Draw(hImage_[14]);


	D3DXMatrixTranslation(&m, (FLOAT)THIEF2 * (g.screenWidth / 3), (FLOAT)g.screenHeight / 3, 0);

	D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

	Image::SetMatrix(hImage_[14], localMatrix_ * a * m);
	Image::Draw(hImage_[14]);
}

void PlayScene::InitTime()
{
	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9] = "1";
	sec_[10] = "0";

	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";
}

void PlayScene::StartGame()
{
	if (!startGame_)
	{
		static int startTime_ = 0;
		D3DXMATRIX m;

		if (startTime_ >= 280)
		{
			startGame_ = true;
			pPolice_->SetPermissionMove(startGame_);
			pThief1_->SetPermissionMove(startGame_);
			pThief2_->SetPermissionMove(startGame_);
			startTime_ = 0;
		}
		else if (startTime_ >= 240)
		{
			for (int i = 0; i < 3; i++)
			{
				D3DXMATRIX a;
				D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

				D3DXMatrixTranslation(&m, (FLOAT)640 * i, (FLOAT)g.screenHeight / 3, 0);

				Image::SetMatrix(hImage_[11], localMatrix_ * m * a);
				Image::Draw(hImage_[11]);
			}
		}
		else if (startTime_ >= 180)
		{
			for (int i = 0; i < 3; i++)
			{
				D3DXMATRIX a;
				D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

				D3DXMatrixTranslation(&m, (FLOAT)640 * i, (FLOAT)g.screenHeight / 3, 0);

				Image::SetMatrix(hImage_[8], localMatrix_ * m * a);
				Image::Draw(hImage_[8]);
			}
		}
		else if (startTime_ >= 120)
		{
			for (int i = 0; i < 3; i++)
			{
				D3DXMATRIX a;
				D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

				D3DXMatrixTranslation(&m, (FLOAT)640 * i, (FLOAT)g.screenHeight / 3, 0);

				Image::SetMatrix(hImage_[9], localMatrix_ * m * a);
				Image::Draw(hImage_[9]);
			}
		}
		else if (startTime_ >= 60)
		{
			for (int i = 0; i < 3; i++)
			{
				D3DXMATRIX a;
				D3DXMatrixScaling(&a, 3.0f, 1.0f, 1.0f);

				D3DXMatrixTranslation(&m, (FLOAT)640 * i, (FLOAT)g.screenHeight / 3, 0);

				Image::SetMatrix(hImage_[10], localMatrix_ * m * a);
				Image::Draw(hImage_[10]);
			}
		}
		startTime_++;

	}

}