#include "Police.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"
#include "Engine/Image.h"
D3DXMATRIX Police::policeView_;
Camera* pCamera;

//コンストラクタ
Police::Police(IGameObject * parent)
	:IGameObject(parent, "Police"), hModel_(-1), radius_(0.5), permissionMove_(false), isTitle_(false), stamina_(100)

{
}

//デストラクタ
Police::~Police()
{
}

//初期化
void Police::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/model/Player/Player_Police_M.fbx");
	assert(hModel_ >= 0);

	SetCollider(D3DXVECTOR3(1, 1, 1), 0.5);

	

	pTempo_ = (Map*)pParent_->FindChildObject("Map");

	circlePos_.PosX = position_.x;
	circlePos_.PosY = position_.z;
	circlePos_.Angle = 180;
	circlePos_.Length = 3.5;
	circlePos_.CenterX = 39.5;
	circlePos_.CenterY = 61.5;

	rotate_.y = 180;

	Model::SetAnimFrame(hModel_, 0, 80, 0.5f);
}

//更新
void Police::Update()
{
	if (isTitle_)
	{
		CreateCircle();
	}
	else
	{
		if (permissionMove_)
		{
			Move();
		}
		WallCollision();

		Transform();

		PoliceView();
	}

	stamina_ += (1 / 60);
}

void Police::CreateCircle()
{
	// 中心座標に角度と長さを使用した円の位置を加算する
	// 度数法の角度を弧度法に変換
	float radius = circlePos_.Angle * 3.14 / 180.0f;

	// 三角関数を使用し、円の位置を割り出す。
	float addX = cos(radius) * circlePos_.Length;
	float addY = sin(radius) * circlePos_.Length;

	// 結果ででた位置を中心位置に加算し、それを描画位置とする
	position_.x = circlePos_.CenterX + addX;
	position_.z = circlePos_.CenterY + addY;

	circlePos_.Angle += 1;
	rotate_.y -= 1;
}

void Police::PoliceView()
{
	D3DXVECTOR3 worldPosition, worldTarget;

	D3DXVec3TransformCoord(&worldPosition, &D3DXVECTOR3(0, 1.0f, 0), &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &D3DXVECTOR3(0, 1.0f, 0.4), &worldMatrix_);

	//ビュー行列を作るための関数(１、入れる箱　２、カメラの位置（視点）３、どこをみるか（焦点） 4上方向ベクトル（上を示す）)
	D3DXMatrixLookAtLH(&view_, &worldPosition, &worldTarget, &D3DXVECTOR3(0, 1, 0));
	//(1,ビュー行列として使ってほしい情報)
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view_);
}

//描画
void Police::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Police::Release()
{
}


void Police::WallCollision()
{
	//チェックするマス
	int checkX;
	int checkZ;

	//左の判定
	checkX = (int)(position_.x - radius_);
	checkZ = (int)position_.z;
	if (pTempo_->IsImPassable(checkX, checkZ))
	{
		position_.x = checkX + 1 + radius_;

	}

	//右の判定
	checkX = (int)(position_.x + radius_);
	checkZ = (int)position_.z;
	if (pTempo_->IsImPassable(checkX, checkZ))
	{
		position_.x = checkX - radius_;
	}

	//奥の判定
	checkX = (int)position_.x;
	checkZ = (int)(position_.z + radius_);
	if (pTempo_->IsImPassable(checkX, checkZ))
	{
		position_.z = checkZ - radius_;
	}

	//後ろの判定
	checkX = (int)position_.x;
	checkZ = (int)(position_.z - radius_);
	if (pTempo_->IsImPassable(checkX, checkZ))
	{
		position_.z = checkZ + 1 + radius_;
	}


}

void Police::Move()
{
	D3DXMATRIX move;
	D3DXMatrixRotationY(&move, D3DXToRadian(rotate_.y));

	//通常移動
	D3DXVECTOR3 UV = D3DXVECTOR3(0, 0, 0.05f);
	D3DXVec3TransformCoord(&UV, &UV, &move);

	D3DXVECTOR3 LR = D3DXVECTOR3(0.05f, 0, 0);
	D3DXVec3TransformCoord(&LR, &LR, &move);

	

	//スティック操作
	D3DXVECTOR3 stick = Input::GetPadStickL(pID_);

	
		

	//////////////////////////////////
	/*  ↓  ここから通常移動   ↓   */
	//////////////////////////////////

	//右斜め前移動
	if (stick.z > 0 && stick.x > 0)
	{
		position_ += UV / 1.5;
		position_ += LR / 1.5;
	}
	//左斜め後ろ移動
	else if (stick.z < 0 && stick.x < 0)
	{
		position_ -= UV / 1.5;
		position_ -= LR / 1.5;
	}
	//左斜め前移動
	else if (stick.z > 0 && stick.x < 0)
	{
		position_ += UV / 1.5;
		position_ -= LR / 1.5;
	}
	//右斜め後ろ移動
	else if (stick.z < 0 && stick.x > 0)
	{
		position_ -= UV / 1.5;
		position_ += LR / 1.5;
	}
	//前移動
	else if (stick.z > 0)
	{
		position_ += UV;
	}
	//後ろ移動
	else if (stick.z < 0)
	{
		position_ -= UV;
	}
	//左移動
	else if (stick.x > 0)
	{
		position_ += LR;
	}
	//右移動
	else if (stick.x < 0)
	{
		position_ -= LR;
	}
	

	{
		//右スティックで回転(視点移動)
		D3DXVECTOR3 stick = Input::GetPadStickR(pID_);

		rotate_.y += (stick.x / 0.65f);
	}

	//→が押されていたら
	if (Input::IsKey(DIK_D))
	{
		//右へ移動
		position_ += LR;

	}

	//←が押されていたら
	if (Input::IsKey(DIK_A))
	{
		//左へ移動
		position_ -= LR;
	}

	//↑が押されていたら
	if (Input::IsKey(DIK_W))
	{
		//上へ移動
		position_ += UV;
	}

	//↓が押されていたら
	if (Input::IsKey(DIK_S))
	{
		//下へ移動
		position_ -= UV;
	}

	//Dキーが押されていたら
	if (Input::IsKey(DIK_E))
	{
		//時計回りに回転
		rotate_.y += 1.0f;
	}

	//Aキーが押されていたら
	if (Input::IsKey(DIK_Q))
	{
		//反時計回りに回転
		rotate_.y -= 1.0f;
	}

}

