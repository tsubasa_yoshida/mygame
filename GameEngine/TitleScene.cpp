#include "TitleScene.h"
#include <string>
#include "Engine/Image.h"
#include "Engine/Audio.h"
#include "Engine/SceneManager.h"
#include "Engine/Direct3D.h"
#include "Map.h"
#include "Police.h"
#include "Thief.h"

using namespace std;

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), alpha_(0)
{
}

//初期化
void TitleScene::Initialize()
{
	//タイトル画像をロード
	hPict_ = Image::Load("Data/Pict/Title_Logo_C.png");
	hPict2_ = Image::Load("Data/pict/Title_PushStart.png");

	CreateGameObject<Map>(this);
	
	//警察を登場させる
	pPolice_ = CreateGameObject<Police>(this);
	pPolice_->SetPosition(D3DXVECTOR3((39.5 - 3.5),0, (61.5 - 3.5)));
	pPolice_->SetTitle(true);

	//泥棒１を登場させる
	pThief1_ = CreateGameObject<Thief>(this);
	pThief1_->SetPosition(D3DXVECTOR3((39.5 + 3.5), 0, (61.5 + 3.5)));
	pThief1_->SetTitle(true);


	pTitleCamera_ = CreateGameObject<TitleCamera>(this);

	Audio::Play("Title_BGM");
}

//更新
void TitleScene::Update()
{
	//Spaceキーでプレイシーンに移動
	if (Input::IsKeyDown(DIK_SPACE))
	{

		SceneManager::ChangeScene(SCENE_ID_PLAY);
	}

	//Spaceキーでプレイシーンに移動
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_START,0))
	{
		SceneManager::ChangeScene(SCENE_ID_PLAY);
	}

	//Spaceキーでプレイシーンに移動
	else if (Input::IsPadButtonDown(XINPUT_GAMEPAD_START,1))
	{
		SceneManager::ChangeScene(SCENE_ID_PLAY);
	}

	//Spaceキーでプレイシーンに移動
	else if (Input::IsPadButtonDown(XINPUT_GAMEPAD_START,2))
	{
		SceneManager::ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	//フェードイン処理
	if (alpha_ != 255)
	{
		alpha_ += 5;
	}
	
	//描画位置を指定
	Image::SetMatrix(hPict_, worldMatrix_);

	//描画
	Image::FadeDraw(hPict_, alpha_);
}

//開放
void TitleScene::Release()
{
	Audio::Stop("Title_BGM");
}

void TitleScene::DrawSub()
{
	//フェードイン処理
	if (alpha_ != 255)
	{
		alpha_ += 5;
	}

	/*for (int i = 0; i < 3; i++)
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, i * 1920, 0, 0);

		Image::SetMatrix(hPict_, localMatrix_ * m);
		Image::FadeDraw(hPict_, alpha_);
	}*/


	for (int i = 0; i < 3; i++)
	{
		D3DXMATRIX m;
		D3DXMATRIX s;
		D3DXMatrixTranslation(&m, (i * 1920) + 200, -300, 0);
		D3DXMatrixScaling(&s, 0.7, 0.7, 0.7);

		Image::SetMatrix(hPict_, localMatrix_ * (s * m));
		Image::FadeDraw(hPict_, alpha_);
	}

	for (int i = 0; i < 3; i++)
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, (i * 1920) , 0, 0);


		Image::SetMatrix(hPict2_, localMatrix_ * m);
		Image::FadeDraw(hPict2_, alpha_);
	}

	//左画面
	{

		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = 0;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//ビュー行列
		Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pTitleCamera_->GetView());

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}

	}

	//中心画面
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = g.screenWidth / 3;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//ビュー行列
		Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pTitleCamera_->GetView());

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}

	}

	//右画面
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = g.screenWidth * 2 / 3;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//ビュー行列
		Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pTitleCamera_->GetView());

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}
	}

	//戻す
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = 0;
		vp.Y = 0;
		vp.Width = g.screenWidth;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);
	}
}
