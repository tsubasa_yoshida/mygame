#pragma once
#include "Global.h"

class Quad
{
protected:
	struct Vertex
	{
		//ポジション	
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};

	Vertex vertexList;

	

	//頂点情報
//使う情報の定義にいってそこの順番通りにしないといけない
//頂点情報などはすぐに取り出しだしたいのでバッファという領域にいれる
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;

	LPDIRECT3DTEXTURE9 pTexture_;

	D3DMATERIAL9         material_;
public:
	Quad();
	~Quad();


	void Load(const char* fileName);
	void Draw(const D3DXMATRIX& matrix);

	
};