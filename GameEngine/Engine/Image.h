#pragma once
#include "Sprite.h"
#include <string>

namespace Image
{
	struct ImageData
	{
		std::string fileName;
		Sprite* pSprite;
		D3DXMATRIX matrix;

		//コンストラクタ
		ImageData() : fileName(""), pSprite(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	//画像読み込み
	//引数 : 読み込む画像のパス
	//戻り値:イメージの識別番号
	int Load(std::string filename);

	//描画関数
	//引数	:読み込むイメージの識別番号 
	//戻り値	:なし
	void FadeDraw(int handle, int alpha);

	//描画関数
	//引数	:読み込むイメージの識別番号 
	//戻り値	:なし
	void Draw(int handle);

	//変形関数
	//引数 :読み込むイメージの識別番号、変形させたい行列
	void SetMatrix(int handle, D3DXMATRIX& matrix);
	
	//開放関数
	//引数	:イメージの識別番号
	//戻り値	:なし
	void Release(int handle);
	
	//開放関数（全部）
	//引数	: なし
	//戻り値 :なし
	void AllRelase();
};

