#pragma once

#include <dInput.h> 
#include "global.h"
#include "XInput.h"

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	void Initialize(HWND hWnd);
	void Update();
	bool IsKey(int keyCode); 
	bool IsKeyDown(int keyCode);
	bool IsKeyUp(int keyCode);	

	///////////////////////////　コントローラー　//////////////////////////////////
	//コントローラーのボタンが押されているか調べる
	//引数：buttonCode	調べたいボタンの番号
	//戻値：押されていればtrue
	bool IsPadButton(int buttonCode, int padID = 0);

	//コントローラーのボタンを今押したか調べる（押しっぱなしは無効）
	//引数：buttonCode	調べたいボタンの番号
	//戻値：押した瞬間だったらtrue
	bool IsPadButtonDown(int buttonCode, int padID = 0);

	//コントローラーのボタンを今放したか調べる
	//引数：buttonCode	調べたいボタンの番号
	//戻値：放した瞬間だったらtrue
	bool IsPadButtonUp(int buttonCode, int padID = 0);

	//左スティックの傾きを取得
	//戻値:傾き具合（-1〜1）
	D3DXVECTOR3 GetPadStickL(int padID = 0);

	//右スティックの傾きを取得
	//戻値:傾き具合（-1〜1）
	D3DXVECTOR3 GetPadStickR(int padID = 0);

	//左トリガーの押し込み具合を取得
	//戻値:押し込み具合（0〜1）
	float		GetPadTrrigerL(int padID = 0);

	//右トリガーの押し込み具合を取得
	//戻値:押し込み具合（0〜1）
	float		GetPadTrrigerR(int padID = 0);

	//振動させる
	void SetPadVibration(int l, int r, int padID = 0);

	void Release();
};