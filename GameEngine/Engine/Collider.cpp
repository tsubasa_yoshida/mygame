#include "Collider.h"



Collider::Collider(IGameObject* owner,D3DXVECTOR3 center, float radius)
{
	owner_ = owner;
	center_ = center;
	radius_ = radius;
}


Collider::~Collider()
{
}

bool Collider::IsHit(Collider& target)
{
	//ベクトル
	D3DXVECTOR3 vLength = (center_ + owner_->GetPosition()) - (target.center_ + target.owner_->GetPosition());
	
	//ベクトルの長さを求める
	float length;
	length = D3DXVec3Length(&vLength);

	//相手のとの距離が二人の半径をより小さいなら
	if (length <= radius_ + target.radius_)
	{
		return true;
	}

	return false;
}
