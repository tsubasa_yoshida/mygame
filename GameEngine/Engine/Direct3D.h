#pragma once

#include "Global.h"

//クラスにはなってないのでグローバル関数
namespace Direct3D
{
	extern LPDIRECT3D9	pD3d;	   //Direct3Dオブジェクト
	extern LPDIRECT3DDEVICE9	pDevice;	//Direct3Dデバイスオブジェクト　<-ゲーム画面をだすために必要な変数

	//初期化
	//引数 : hwnd ウィンドウハンドル
	//戻値 : なし
	void Initialize(HWND hWnd);

	//描画開始
	//引数 : なし
	//戻値 : なし
	void BeginDraw();

	//描画終了
	//引数 : なし
	//戻値 : なし
	void EndDraw();

	//開放処理
	//引数 : なし
	//戻値 : なし
	void Release();
}
