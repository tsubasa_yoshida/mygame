#include "IGameObject.h"
#include "SceneManager.h"
#include "Collider.h"

//コンストラクタ(親も名前もなし)
IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
}

//コンストラクタ（名前なし）
IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")
{
}

//コンストラクタ（標準）
IGameObject::IGameObject(IGameObject * parent, const std::string & name) :
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)),
	pCollider_(nullptr),dead_(false)
{
	
}

void IGameObject::Transform()
{
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;

	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	//拡大縮小、回転、移動
	localMatrix_ = matS * matRX * matRY *matRZ * matT;

	if (pParent_ == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
	
}

IGameObject::~IGameObject()
{
	SAFE_DELETE(pCollider_);
}



//子供の情報を習得
void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}

void IGameObject::UpdateSub()
{
	Update();
	
	Transform();

	Collision(SceneManager::GetCurrentScene());

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		//死亡フラグが立っていたらそいつを殺す
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
	}
}

void IGameObject::DrawSub()
{

	Draw();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	dead_ = true;
}

void IGameObject::SetCollider(D3DXVECTOR3 &center, float radius)
{
	pCollider_ = new Collider(this,center,radius);
}

void IGameObject::Collision(IGameObject* targetOblect)
{
	
	if (targetOblect != this &&
		this->pCollider_ != nullptr &&
		targetOblect->pCollider_ != nullptr &&
		pCollider_->IsHit(*targetOblect->pCollider_))
	{
		OnCollision(targetOblect);
	}
	

	for(auto itr = targetOblect->childList_.begin();
		itr != targetOblect->childList_.end();itr++)
	{
		Collision(*itr);
	}
}

const std::string & IGameObject::GetObjectName(void) const
{
	return name_;
}

IGameObject * IGameObject::GetParent()
{
	return pParent_;
}

IGameObject * IGameObject::FindChildObject(const std::string & name)
{

	//子供がいないなら終わり
	if (childList_.empty())
		return nullptr;

	//イテレータ
	auto it = childList_.begin();	//先頭
	auto end = childList_.end();	//末尾

	//子オブジェクトから探す
	while (it != end) {
		//同じ名前のオブジェクトを見つけたらそれを返す
		if ((*it)->GetObjectName() == name)
			return *it;

		//その子供（孫）以降にいないか探す
		IGameObject* obj = (*it)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}

		//次の子へ
		it++;
	}

	//見つからなかった
	return nullptr;
}
