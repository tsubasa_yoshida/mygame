#include <Windows.h>
#include "Global.h"
#include "Direct3D.h"
#include "RootJob.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"
#include "Movie.h"

//メモリーリーク検出用
#ifdef _DEBUG
#include <crtdbg.h>
#endif

//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")


//グローバル変数
const char* WIN_CLASS_NAME = "SampleGame";
const int	WINDOW_WIDTH  = 1920 * 3;	//ウィンドウの幅
const int	WINDOW_HEIGHT = 1080;	//ウィンドウの高さ

Global g;
RootJob *pRootJob;

//LP = ロングポインタ
//頭文字I　= インターフェース
//インターフェースは実態をおこせない

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

//H = 識別番号　
//INSTANCE	= なんらかの規格のよって作られたもの
//hPrevInst = 昔の名残らしい無視してよい
//lpCmdLine	= ゲームでは使わない　ドラックして起動する方法
//nCmdShow　= 今は考えなくてよい

//エントリーポイント　
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	
#ifdef _DEBUG
	// メモリリーク検出
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//ウィンドウクラス（設計図）を作成
	//wc の中に情報が入っていく
	WNDCLASSEX wc;

	wc.cbSize = sizeof(WNDCLASSEX);				//この構造体のサイズ　= サイズってきたらこの構造体の大きさを指定しろ（Sizeof）の合図らしい　メモリの使う量教えてという意味らしい
	wc.hInstance = hInstance;					//インスタンスハンドル = 上のインスタンスハンドルの番号そのままいれるらしい	
	wc.lpszClassName = WIN_CLASS_NAME;			//ウィンドウクラス名　			　今から作ろうとしている設計図の名前らしい
	wc.lpfnWndProc = WndProc;					//ウィンドウプロシージャ			　ウィンドウプロシージャの名前を書くなにか動いたときにどれを使うのか　上でプロトタイプ宣言したのもこのためらしい
	wc.style = CS_VREDRAW | CS_HREDRAW;			//スタイル（デフォルト）	　　　　気にしなくていいおきまり
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);	//アイコン　IDI_〜で変えれらる　カラー可
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);	//小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);	//マウスカーソル マウスをカーソルを変えれる　ゲーム内のカラフルなカーソルはウィンドウズのカーソルを透明にしてそこの位置に画像を表示させる　
	wc.lpszMenuName = NULL;						//メニュー（なし）
	wc.cbClsExtra = 0;							//ここは使わない
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);		//背景（白）ゲームは背景だすからあんまり関係ない

	RegisterClassEx(&wc);										//クラスを登録　ここでいれた設計図をつかえるようにする

	RECT winRect{ 0,0,WINDOW_WIDTH,WINDOW_HEIGHT };				//ウィンドウの余白のせいで画像がはみでる可能性がある
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);		//ここで幅をぴったり合わせる

	//ウィンドウを作成
	//wnd = ウィンドウ
	//hwnd　=　ウィンドウハンドル　ウィンドウの識別番号が入る
	HWND hWnd = CreateWindow(			
										//CreateWindow　ウィンドウ作成関数
		WIN_CLASS_NAME,					//ウィンドウクラス名　上と同じにするどれを使うのかをしていする　設計図は複数作成できる
		"Police & Thief",    				//タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,			//スタイル（普通のウィンドウ）フルスクリーンとかにはWS_POPUPがよい　ゲームははWS_SYSMENUが多いらしい
		CW_USEDEFAULT,					//表示位置左（おまかせ）
		CW_USEDEFAULT,					//表示位置上（おまかせ）
		winRect.right - winRect.left,	//ウィンドウ幅  
		winRect.bottom - winRect.top,	//ウィンドウ高さ
		NULL,							//親ウインドウ（なし）ゲーム開発だとたいしてつかわないらしい
		NULL,							//メニュー（なし）
		hInstance,						//インスタンス
		NULL							//パラメータ（なし）
	);
	assert(hWnd != NULL);

										//ウィンドウを表示
										//ここを書かないと透明のまま
	ShowWindow(hWnd, nCmdShow);

	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	/////////////// 初期化 ///////////////
	Direct3D::Initialize(hWnd);

	Input::Initialize(hWnd);

	Audio::Initialize();
	Audio::WaveBankLoad("Data/music/WaveBank.xwb");
	Audio::SoundBankLoad("Data/music/SoundBank.xsb");

	Movie::Initialize(hWnd);

	pRootJob = new RootJob;
	pRootJob->Initialize();

	MSG msg;
	ZeroMemory(&msg, sizeof(msg));

	//////////メッセージループ（何か起きるのを待つ）//////////

	while (msg.message != WM_QUIT)//もしプログラムが終了したら抜ける
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//入力情報の更新
			Input::Update();

			//ゲームの更新
			pRootJob->UpdateSub();

			//描画開始
			Direct3D::BeginDraw();
			pRootJob->DrawSub();

			//描画終了
			Direct3D::EndDraw();
			
		}
	}

	//開放処理
	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Audio::Release();
	Model::AllRelease();
	Image::AllRelase();
	Input::Release();	
	Direct3D::Release();

	return 0;
}

//msg メッセージ　	何かがおきたら何かがはいる　クリックなど
//wParam msg の追加情報　マウスの操作でmsgが呼ばれたらその追加情報がはいる
//ウィンドウの×ボタンが押された（WM_DESTROY）場合はプログラムを終了。
//何かあったかの判断はウィンドウごとに判断する
//ウィンドウプロシージャ（何かあった時によばれる関数
//CALLBACK　何か呼ばれたときに呼ぶときに使う関数
//LRSURT はここ以外で見たことないらしい　
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//何がおきたかをスイッチ文で分岐
	//なにかおきたかで動くプログラムを	メッセージ駆動型　というゲームには向かない

	switch (msg)
	{
	case WM_DESTROY://ウィンドウの×が押されたら〜
		PostQuitMessage(0);	//プログラム終了
		return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);//本来ウィンドウプロシージャはすべての処理にたいしてなんらかの処理をしないいけないとくに処理がないときはここがやってくれる
}