#pragma once
#include<d3dx9.h>
#include<list>
#include<string>


//ヘッダーでクラスを使いたいけどインクルードできないときは
//クラスの前方宣言
class Player;

class Collider;

struct DrawObject
{
	float PosX;		// 描画座標X
	float PosY;		// 描画座標Y
	float Radius;		// 半径(描画用)
	float CenterX;	// 中心座標X
	float CenterY;	// 中心座標Y
	float Angle;		// 角度
	float Length;		// 半径の長さ
};

class IGameObject
{
protected:	
	//親の情報
	IGameObject* pParent_;
	
	Collider* pCollider_;

	//子供の情報
	std::list<IGameObject*> childList_;
	
	//オブジェクトの名前
	std::string name_;

	//位置、角度、大きさ
	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	//自分の行列用
	D3DXMATRIX localMatrix_;

	//親のワールドの行列をかけた行列
	D3DXMATRIX worldMatrix_;

	//死ぬかどうかのフラグ
	bool dead_;

	void Transform();

public:
	//コンストラクタ
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);
	//デストラクタ
	virtual ~IGameObject();

	template <class T>
	T* CreateGameObject(IGameObject* parent)
	{
		//親の情報を取得
		T* p = new T(parent);
		//親の子供情報にこれをいれる
		parent->PushBackChild(p);
		p->Initialize();
		return p;
	}

	//

	//子供の情報を使いする変数
	void PushBackChild(IGameObject* obj);
	//初期化
	virtual void Initialize() = 0;
	//更新
	virtual void Update() = 0;
	//描画
	virtual void Draw() = 0;
	//開放
	virtual void Release() = 0;

	//子クラス用
	void UpdateSub();
	virtual void DrawSub();
	void ReleaseSub();

	//自分が死ぬことを教える
	void KillMe();
	
	D3DXVECTOR3 GetPosition() {return position_;}

	D3DXVECTOR3 GetRotate() {return rotate_;}

	D3DXVECTOR3 GetScale() {return scale_;}

	void SetPosition(D3DXVECTOR3 position) {position_ = position;}

	void SetCollider(D3DXVECTOR3 &center, float radius);

	void Collision(IGameObject* targetOblect);

	const std::string& GetObjectName(void) const;

	IGameObject* GetParent();

	//あたり判定時の処理用仮想関数CLEAR
	virtual void OnCollision(IGameObject* pTarget) {};

	IGameObject* FindChildObject(const std::string& name);



};

