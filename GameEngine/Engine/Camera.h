#pragma once
#include "IGameObject.h"


//◆◆◆を管理するクラス
class Camera : public IGameObject
{
	//焦点
	D3DXVECTOR3 target_;
	int pID_;
public:
	static D3DXMATRIX cameraView_;

	//コンストラクタ
	Camera(IGameObject* parent);

	//デストラクタ
	~Camera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ターゲットのセッター
	//引数	:指定されたターゲット
	//戻り値	:なし
	void SetTarget(D3DXVECTOR3 target)
	{
		target_ = target;
	}

	void SetPlayerID(int id) { pID_ = id; }
};