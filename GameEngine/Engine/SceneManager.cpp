#include "SceneManager.h"
#include "Global.h"
#include "../PlayScene.h"
#include "../SplashScene.h"
#include "../TitleScene.h"
#include "../PoliceWinResult.h"
#include "../ThiefWinResult.h"

SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_PLAY;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_PLAY;
IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{
}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{

	pCurrentScene_ = CreateGameObject<SplashScene>(this);
}

//更新
void SceneManager::Update()
{

	//今のIDと次のシーンが違うならシーンを切り替える
	if (currentSceneID_ != nextSceneID_)
	{
		//一番最初の子供を見つける
		auto scene = childList_.begin();
		//一番若いやつから殺していく
		(*scene)->ReleaseSub();
		//最後に最初の子供を消す
		SAFE_DELETE(*scene);
		
		childList_.clear();

		switch (nextSceneID_)
		{
		case SCENE_ID_PLAY: 
		pCurrentScene_ = CreateGameObject<PlayScene>(this); break;
		case SCENE_ID_TITLE:
		pCurrentScene_ = CreateGameObject<TitleScene>(this); break;
		case SCENE_ID_SPLASH:
		pCurrentScene_ = CreateGameObject<SplashScene>(this); break;
		case SCENE_ID_POLICEWIN:
		pCurrentScene_ = CreateGameObject<PoliceWinResult>(this); break;
		case SCENE_ID_THIEFWIN:
		pCurrentScene_ = CreateGameObject<ThiefWinResult>(this); break;
		}
		//現在のシーンを次のシーンに設定
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{
}

//開放
void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
